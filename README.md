# 离线数仓项目

## 1 项目简介

​	基于电商平台的日志数据及业务数据，在Hive中搭建离线数仓，为后续的决策提供数据支持。

## 2 项目架构

![1.离线数仓项目架构.drawio](image/1.离线数仓项目架构.png)

## 3 集群资源规划

| 服务名称               | 子服务               | 服务器hadoop102 | 服务器hadoop103 | 服务器hadoop104 |
| ---------------------- | -------------------- | --------------- | --------------- | --------------- |
| HDFS                   | NameNode             | √               |                 |                 |
|                        | DataNode             | √               | √               | √               |
|                        | SecondaryNameNode    |                 |                 | √               |
| YARN                   | NodeManager          | √               | √               | √               |
|                        | ResourceManager      |                 | √               |                 |
| ZooKeeper              | ZooKeeperServer      | √               | √               | √               |
| Flume（采集日志）      | Flume                | √               | √               |                 |
| Kafka                  | Kafka                | √               | √               | √               |
| Flume（消费Kafka日志） | Flume                |                 |                 | √               |
| Flume（消费Kafka业务） | Flume                |                 |                 | √               |
| Hive                   | Hive                 | √               |                 |                 |
| MySQL                  | MySQL                | √               |                 |                 |
| DataX                  | DataX                | √               |                 |                 |
| Maxwell                | Maxwell              | √               |                 |                 |
| Spark                  |                      | √               | √               | √               |
| DolphinScheduler       | ApiApplicationServer | √               |                 |                 |
|                        | AlertServer          | √               |                 |                 |
|                        | WorkerServer         | √               | √               | √               |
|                        | LoggerServer         | √               | √               | √               |
| Superset /   Fine BI   |                      | √               |                 |                 |

## 4 DolphinScheduler脚本调度顺序

![3.脚本调度顺序.drawio](image/2.脚本调度顺序.png)