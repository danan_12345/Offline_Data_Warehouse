/*
    DWD层建表说明：
        由于DWD层会被频繁的查询，为了提高查询效率，保存文件格式选择列式存储ORC，压缩方式选择速度较快的snappy

    分区说明：
        首日数据包含历史数据，需要动态分区
        每日数据只含当天的数据，一般不需要动态分区
*/

-- 1 交易域加购事务事实表
DROP TABLE IF EXISTS dwd_trade_cart_add_inc;
CREATE EXTERNAL TABLE dwd_trade_cart_add_inc (
    `id`               STRING COMMENT '编号',
    `user_id`          STRING COMMENT '用户id',
    `sku_id`           STRING COMMENT '商品id',
    `date_id`          STRING COMMENT '时间id',
    `create_time`      STRING COMMENT '加购时间',
    `source_id`        STRING COMMENT '来源类型ID',
    `source_type_code` STRING COMMENT '来源类型编码',
    `source_type_name` STRING COMMENT '来源类型名称',
    `sku_num`          BIGINT COMMENT '加购物车件数'

) COMMENT '交易域加购事务事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_trade_cart_add_inc/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
-- 首日（所有bootstrap-insert的数据都是加购数据，动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
with
cart as (
    select data.id,
           data.user_id,
           data.sku_id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time,
           data.source_id,
           data.source_type,
           data.sku_num
    from ods_cart_info_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),dic24 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-14' and parent_code='24'
)
insert overwrite table dwd_trade_cart_add_inc partition (dt)
select id,
       user_id,
       sku_id,
       date_id,
       create_time,
       source_id,
       source_type,
       dic24.dic_name,
       sku_num,
       date_id dt
from cart
left join dic24 on cart.source_type = dic24.dic_code;
-- 每日（所有insert的数据 + update之后的加购数量比旧的数量大）
with
cart as (
    select data.id,
           data.user_id,
           data.sku_id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time,
           data.source_id,
           data.source_type,
           data.sku_num
    from ods_cart_info_inc
    where dt='2023-01-15'
    and (type='insert' or (type='update' and old['sku_num'] is not null and data.sku_num > cast(old['sku_num'] as int)))
),dic24 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-15' and parent_code='24'
)
insert overwrite table dwd_trade_cart_add_inc partition (dt='2023-01-15')
select id,
       user_id,
       sku_id,
       date_id,
       create_time,
       source_id,source_type,
       dic24.dic_name,
       sku_num
from cart
left join dic24 on cart.source_type = dic24.dic_code;

-- 2 交易域下单事务事实表
DROP TABLE IF EXISTS dwd_trade_order_detail_inc;
CREATE EXTERNAL TABLE dwd_trade_order_detail_inc (
    `id`                    STRING COMMENT '编号',
    `order_id`              STRING COMMENT '订单id',
    `user_id`               STRING COMMENT '用户id',
    `sku_id`                STRING COMMENT '商品id',
    `province_id`           STRING COMMENT '省份id',
    `activity_id`           STRING COMMENT '参与活动规则id',
    `activity_rule_id`      STRING COMMENT '参与活动规则id',
    `coupon_id`             STRING COMMENT '使用优惠券id',
    `date_id`               STRING COMMENT '下单日期id',
    `create_time`           STRING COMMENT '下单时间',
    `source_id`             STRING COMMENT '来源编号',
    `source_type_code`      STRING COMMENT '来源类型编码',
    `source_type_name`      STRING COMMENT '来源类型名称',
    `sku_num`               BIGINT COMMENT '商品数量',
    `split_original_amount` DECIMAL(16, 2) COMMENT '原始价格',
    `split_activity_amount` DECIMAL(16, 2) COMMENT '活动优惠分摊',
    `split_coupon_amount`   DECIMAL(16, 2) COMMENT '优惠券优惠分摊',
    `split_total_amount`    DECIMAL(16, 2) COMMENT '最终价格分摊'
) COMMENT '交易域下单事务事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_trade_order_detail_inc/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
-- 首日（所有bootstrap-insert数据都是下单数据，动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
with
od as (
    select data.id,
           data.order_id,
           data.sku_id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time,
           data.source_id,
           data.source_type,
           data.sku_num,
           data.sku_num * data.order_price split_original_amount,
           data.split_activity_amount,
           data.split_coupon_amount,
           data.split_total_amount
    from ods_order_detail_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),oi as (
    select data.id,
           data.user_id,
           data.province_id
    from ods_order_info_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),oa as (
    select order_detail_id,
           activity_id,
           activity_rule_id
    from (
             select data.order_detail_id,
                    data.activity_id,
                    data.activity_rule_id,
                    row_number() over (partition by data.order_detail_id) rn
             from ods_order_detail_activity_inc
             where dt = '2023-01-14'
               and type = 'bootstrap-insert'
    ) t1
    where rn=1
),oc as (
    select distinct data.order_detail_id,
           data.coupon_id
    from ods_order_detail_coupon_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),dic24 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-14' and parent_code='24'
)
insert overwrite table dwd_trade_order_detail_inc partition(dt)
select od.id,
       od.order_id,
       oi.user_id,
       od.sku_id,
       oi.province_id,
       oa.activity_id,
       oa.activity_rule_id,
       oc.coupon_id,
       od.date_id,
       od.create_time,
       od.source_id,
       od.source_type,
       dic24.dic_name,
       od.sku_num,
       od.split_original_amount,
       od.split_activity_amount,
       od.split_coupon_amount,
       od.split_total_amount,
       od.date_id dt
from od
left join oi on od.order_id = oi.id
left join oa on od.id = oa.order_detail_id
left join oc on od.id = oc.order_detail_id
left join dic24 on od.source_type = dic24.dic_code;
-- 每日（所有insert的数据都是下单数据）
with
od as (
    select data.id,
           data.order_id,
           data.sku_id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time,
           data.source_id,
           data.source_type,
           data.sku_num,
           data.sku_num * data.order_price split_original_amount,
           data.split_activity_amount,
           data.split_coupon_amount,
           data.split_total_amount
    from ods_order_detail_inc
    where dt='2023-01-15' and type='insert'
),oi as (
    select data.id,
           data.user_id,
           data.province_id
    from ods_order_info_inc
    where dt='2023-01-15' and type='insert'
),oa as (
    select order_detail_id,
           activity_id,
           activity_rule_id
    from (
             select data.order_detail_id,
                    data.activity_id,
                    data.activity_rule_id,
                    row_number() over (partition by data.order_detail_id) rn
             from ods_order_detail_activity_inc
             where dt = '2023-01-15'
               and type = 'insert'
    ) t1
    where rn=1
),oc as (
    select distinct data.order_detail_id,
           data.coupon_id
    from ods_order_detail_coupon_inc
    where dt='2023-01-15' and type='insert'
),dic24 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-15' and parent_code='24'
)
insert overwrite table dwd_trade_order_detail_inc partition (dt='2023-01-15')
select od.id,
       od.order_id,
       oi.user_id,
       od.sku_id,
       oi.province_id,
       oa.activity_id,
       oa.activity_rule_id,
       oc.coupon_id,
       od.date_id,
       od.create_time,
       od.source_id,
       od.source_type,
       dic24.dic_name,
       od.sku_num,
       od.split_original_amount,
       od.split_activity_amount,
       od.split_coupon_amount,
       od.split_total_amount
from od
left join oi on od.order_id = oi.id
left join oa on od.id = oa.order_detail_id
left join oc on od.id = oc.order_detail_id
left join dic24 on od.source_type = dic24.dic_code;

-- 3 交易域支付成功事务事实表
DROP TABLE IF EXISTS dwd_trade_pay_detail_suc_inc;
CREATE EXTERNAL TABLE dwd_trade_pay_detail_suc_inc (
    `id`                    STRING COMMENT '编号',
    `order_id`              STRING COMMENT '订单id',
    `user_id`               STRING COMMENT '用户id',
    `sku_id`                STRING COMMENT '商品id',
    `province_id`           STRING COMMENT '省份id',
    `activity_id`           STRING COMMENT '参与活动规则id',
    `activity_rule_id`      STRING COMMENT '参与活动规则id',
    `coupon_id`             STRING COMMENT '使用优惠券id',
    `payment_type_code`     STRING COMMENT '支付类型编码',
    `payment_type_name`     STRING COMMENT '支付类型名称',
    `date_id`               STRING COMMENT '支付日期id',
    `callback_time`         STRING COMMENT '支付成功时间',
    `source_id`             STRING COMMENT '来源编号',
    `source_type_code`      STRING COMMENT '来源类型编码',
    `source_type_name`      STRING COMMENT '来源类型名称',
    `sku_num`               BIGINT COMMENT '商品数量',
    `split_original_amount` DECIMAL(16, 2) COMMENT '应支付原始金额',
    `split_activity_amount` DECIMAL(16, 2) COMMENT '支付活动优惠分摊',
    `split_coupon_amount`   DECIMAL(16, 2) COMMENT '支付优惠券优惠分摊',
    `split_payment_amount`  DECIMAL(16, 2) COMMENT '支付金额'
) COMMENT '交易域支付成功事务事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_trade_pay_detail_suc_inc/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
-- 首日（payment_status为‘1602’）
set hive.exec.dynamic.partition.mode=nonstrict;
with
pay as (
    select data.order_id,
           data.user_id,
           data.payment_type,
           data.callback_time,
           date_format(data.callback_time,'yyyy-MM-dd') date_id
    from ods_payment_info_inc
    where dt='2023-01-14' and type='bootstrap-insert' and data.payment_status='1602'
),oi as (
    select data.id,
           data.province_id
    from ods_order_info_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),od as (
    select data.id,
           data.order_id,
           data.sku_id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time,
           data.source_id,
           data.source_type,
           data.sku_num,
           data.sku_num * data.order_price split_original_amount,
           data.split_activity_amount,
           data.split_coupon_amount,
           data.split_total_amount
    from ods_order_detail_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),oa as (
    select order_detail_id,
           activity_id,
           activity_rule_id
    from (
             select data.order_detail_id,
                    data.activity_id,
                    data.activity_rule_id,
                    row_number() over (partition by data.order_detail_id) rn
             from ods_order_detail_activity_inc
             where dt = '2023-01-14'
               and type = 'bootstrap-insert'
    ) t1
    where rn=1
),oc as (
    select distinct data.order_detail_id,
           data.coupon_id
    from ods_order_detail_coupon_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),dic24 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-14' and parent_code='24'
),dic11 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-14' and parent_code='11'
)
insert overwrite table dwd_trade_pay_detail_suc_inc partition (dt)
select t1.id,
       t1.order_id,
       t1.user_id,
       t1.sku_id,
       oi.province_id,
       oa.activity_id,
       oa.activity_rule_id,
       oc.coupon_id,
       t1.payment_type,
       dic11.dic_name,
       t1.date_id,
       t1.callback_time,
       t1.source_id,
       t1.source_type,
       dic24.dic_name,
       t1.sku_num,
       t1.split_original_amount,
       t1.split_activity_amount,
       t1.split_coupon_amount,
       t1.split_total_amount,
       t1.date_id dt
from (
         select od.id,
                od.order_id,
                pay.user_id,
                od.sku_id,
                pay.payment_type,
                pay.date_id,
                pay.callback_time,
                od.source_id,
                od.source_type,
                od.sku_num,
                od.split_original_amount,
                od.split_activity_amount,
                od.split_coupon_amount,
                od.split_total_amount
         from od
         join pay on od.order_id = pay.order_id
) t1
left join oi on t1.order_id = oi.id
left join oa on t1.id = oa.order_detail_id
left join oc on t1.id = oc.coupon_id
left join dic11 on t1.payment_type = dic11.dic_code
left join dic24 on t1.source_type = dic24.dic_code;
-- 每日（update的数据）
/*
    注意：下单 -> 支付，时限为24小时
*/
with
pay as (
    select data.order_id,
           data.user_id,
           data.payment_type,
           data.callback_time,
           date_format(data.callback_time,'yyyy-MM-dd') date_id
    from ods_payment_info_inc
    where dt='2023-01-15' and type='update' and data.payment_status='1602' and array_contains(map_keys(old),'payment_status')
),oi as (
    select data.id,
           data.province_id
    from ods_order_info_inc
    where (dt='2023-01-15' or dt=date_sub('2023-01-15',1)) and (type='bootstrap-insert' or type='insert')
),od as (
    select data.id,
           data.order_id,
           data.sku_id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time,
           data.source_id,
           data.source_type,
           data.sku_num,
           data.sku_num * data.order_price split_original_amount,
           data.split_activity_amount,
           data.split_coupon_amount,
           data.split_total_amount
    from ods_order_detail_inc
    where (dt='2023-01-15' or dt=date_sub('2023-01-15',1)) and (type='bootstrap-insert' or type='insert')
),oa as (
    select order_detail_id,
           activity_id,
           activity_rule_id
    from (
             select data.order_detail_id,
                    data.activity_id,
                    data.activity_rule_id,
                    row_number() over (partition by data.order_detail_id) rn
             from ods_order_detail_activity_inc
             where (dt='2023-01-15' or dt=date_sub('2023-01-15',1)) and (type='bootstrap-insert' or type='insert')
    ) t1
    where rn=1
),oc as (
    select distinct data.order_detail_id,
           data.coupon_id
    from ods_order_detail_coupon_inc
    where (dt='2023-01-15' or dt=date_sub('2023-01-15',1)) and (type='bootstrap-insert' or type='insert')
),dic24 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-15' and parent_code='24'
),dic11 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-15' and parent_code='11'
)
insert overwrite table dwd_trade_pay_detail_suc_inc partition (dt='2023-01-15')
select t1.id,
       t1.order_id,
       t1.user_id,
       t1.sku_id,
       oi.province_id,
       oa.activity_id,
       oa.activity_rule_id,
       oc.coupon_id,
       t1.payment_type,
       dic11.dic_name,
       t1.date_id,
       t1.callback_time,
       t1.source_id,
       t1.source_type,
       dic24.dic_name,
       t1.sku_num,
       t1.split_original_amount,
       t1.split_activity_amount,
       t1.split_coupon_amount,
       t1.split_total_amount
from (
         select od.id,
                od.order_id,
                pay.user_id,
                od.sku_id,
                pay.payment_type,
                pay.date_id,
                pay.callback_time,
                od.source_id,
                od.source_type,
                od.sku_num,
                od.split_original_amount,
                od.split_activity_amount,
                od.split_coupon_amount,
                od.split_total_amount
         from od
         join pay on od.order_id = pay.order_id
) t1
left join oi on t1.order_id = oi.id
left join oa on t1.id = oa.order_detail_id
left join oc on t1.id = oc.coupon_id
left join dic11 on t1.payment_type = dic11.dic_code
left join dic24 on t1.source_type = dic24.dic_code;

-- 4 交易域购物车周期快照事实表
/*
    周期快照事实表：常用于分析存量型指标
*/
DROP TABLE IF EXISTS dwd_trade_cart_full;
CREATE EXTERNAL TABLE dwd_trade_cart_full (
    `id`       STRING COMMENT '编号',
    `user_id`  STRING COMMENT '用户id',
    `sku_id`   STRING COMMENT '商品id',
    `sku_name` STRING COMMENT '商品名称',
    `sku_num`  BIGINT COMMENT '加购物车件数'
) COMMENT '交易域购物车周期快照事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_trade_cart_full/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
insert overwrite table dwd_trade_cart_full partition (dt='2023-01-14')
select id,
       user_id,
       sku_id,
       sku_name,
       sku_num
from ods_cart_info_full
where dt='2023-01-14';

-- 5 交易域交易流程累计快照事实表
/*
    累计快照事实表：常用于分析多事务关联统计需求
*/
DROP TABLE IF EXISTS dwd_trade_trade_flow_acc;
CREATE EXTERNAL TABLE dwd_trade_trade_flow_acc (
    `order_id`              STRING COMMENT '订单id',
    `user_id`               STRING COMMENT '用户id',
    `province_id`           STRING COMMENT '省份id',
    `order_date_id`         STRING COMMENT '下单日期id',
    `order_time`            STRING COMMENT '下单时间',
    `payment_date_id`       STRING COMMENT '支付日期id',
    `payment_time`          STRING COMMENT '支付时间',
    `finish_date_id`        STRING COMMENT '确认收货日期id',
    `finish_time`           STRING COMMENT '确认收货时间',
    `order_original_amount` DECIMAL(16, 2) COMMENT '下单原始价格',
    `order_activity_amount` DECIMAL(16, 2) COMMENT '下单活动优惠分摊',
    `order_coupon_amount`   DECIMAL(16, 2) COMMENT '下单优惠券优惠分摊',
    `order_total_amount`    DECIMAL(16, 2) COMMENT '下单最终价格分摊',
    `payment_amount`        DECIMAL(16, 2) COMMENT '支付金额'
) COMMENT '交易域交易流程累计快照事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_trade_trade_flow_acc/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
/*
    分区说明(动态分区)：
        当日分区：存储当天已完成的订单信息
        9999-99-99分区：存储未完成的订单信息
*/
-- 首日
set hive.exec.dynamic.partition.mode=nonstrict;
with
oi as (
    select data.id,
           data.user_id,
           data.province_id,
           data.create_time,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.original_total_amount,
           data.activity_reduce_amount,
           data.coupon_reduce_amount,
           data.total_amount
    from ods_order_info_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),pay as (
    select data.order_id,
           data.callback_time,
           date_format(data.callback_time,'yyyy-MM-dd') payment_date_id,
           data.total_amount
    from ods_payment_info_inc
    where dt='2023-01-14' and type='bootstrap-insert' and data.payment_status='1602'
),os as (
    select data.order_id,
           data.operate_time,
           date_format(data.operate_time,'yyyy-MM-dd') finish_date_id
    from ods_order_status_log_inc
    where dt='2023-01-14' and type='bootstrap-insert' and data.order_status='1004'
)
insert overwrite table dwd_trade_trade_flow_acc partition (dt)
select oi.id,
       oi.user_id,
       oi.province_id,
       oi.date_id,
       oi.create_time,
       pay.payment_date_id,
       pay.callback_time,
       os.finish_date_id,
       os.operate_time,
       oi.original_total_amount,
       oi.activity_reduce_amount,
       oi.coupon_reduce_amount,
       oi.total_amount,
       pay.total_amount,
       nvl(os.finish_date_id,'9999-99-99') dt
from oi
left join pay on oi.id = pay.order_id
left join os on oi.id = os.order_id;
-- 每日
set hive.exec.dynamic.partition.mode=nonstrict;
with
old as (
   select order_id,
          user_id,
          province_id,
          order_date_id,
          order_time,
          payment_date_id,
          payment_time,
          finish_date_id,
          finish_time,
          order_original_amount,
          order_activity_amount,
          order_coupon_amount,
          order_total_amount,
          payment_amount
   from dwd_trade_trade_flow_acc
    where dt='9999-99-99'
),oi as (
    select data.id,
           data.user_id,
           data.province_id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time,
           null payment_date_id,
           null payment_time,
           null finish_date_id,
           null finish_time,
           data.original_total_amount,
           data.activity_reduce_amount,
           data.coupon_reduce_amount,
           data.total_amount,
           null payment_amount
    from ods_order_info_inc
    where dt='2023-01-15' and type='insert'
),pay as (
    select data.order_id,
           data.callback_time,
           date_format(data.callback_time,'yyyy-MM-dd') payment_date_id,
           data.total_amount
    from ods_payment_info_inc
    where dt='2023-01-15' and type='update' and array_contains(map_keys(old),'payment_status') and data.payment_status='1602'
),os as (
    select data.order_id,
           data.operate_time,
           date_format(data.operate_time,'yyyy-MM-dd') finish_date_id
    from ods_order_status_log_inc
    where dt='2023-01-15' and type='insert' and data.order_status='1004'
)
insert overwrite table dwd_trade_trade_flow_acc partition (dt)
select t1.order_id,
       t1.user_id,
       t1.province_id,
       t1.order_date_id,
       t1.order_time,
       nvl(t1.payment_date_id,pay.payment_date_id),
       nvl(t1.payment_time,pay.callback_time),
       nvl(t1.finish_date_id,os.finish_date_id),
       nvl(t1.finish_time,os.operate_time),
       t1.order_original_amount,
       t1.order_activity_amount,
       t1.order_coupon_amount,
       t1.order_total_amount,
       nvl(t1.payment_amount,pay.total_amount),
       nvl(os.finish_date_id,'9999-99-99') dt
from (
         select order_id,
                user_id,
                province_id,
                order_date_id,
                order_time,
                payment_date_id,
                payment_time,
                finish_date_id,
                finish_time,
                order_original_amount,
                order_activity_amount,
                order_coupon_amount,
                order_total_amount,
                payment_amount
         from old
         union all
         select id,
                user_id,
                province_id,
                date_id,
                create_time,
                payment_date_id,
                payment_time,
                finish_date_id,
                finish_time,
                original_total_amount,
                activity_reduce_amount,
                coupon_reduce_amount,
                total_amount,
                payment_amount
         from oi
) t1
left join pay on t1.order_id = pay.order_id
left join os on t1.order_id = os.order_id;

-- 6 工具域优惠券使用（支付）事务事实表
DROP TABLE IF EXISTS dwd_tool_coupon_used_inc;
CREATE EXTERNAL TABLE dwd_tool_coupon_used_inc
(
    `id`           STRING COMMENT '编号',
    `coupon_id`    STRING COMMENT '优惠券ID',
    `user_id`      STRING COMMENT 'user_id',
    `order_id`     STRING COMMENT 'order_id',
    `date_id`      STRING COMMENT '日期ID',
    `payment_time` STRING COMMENT '使用下单时间'
) COMMENT '优惠券使用支付事务事实表'
    PARTITIONED BY (`dt` STRING)
    STORED AS ORC
    LOCATION '/warehouse/gmall/dwd/dwd_tool_coupon_used_inc/'
    TBLPROPERTIES ("orc.compress" = "snappy");
-- 数据装载
-- 首日
set hive.exec.dynamic.partition.mode=nonstrict;
insert overwrite table dwd_tool_coupon_used_inc partition (dt)
select data.id,
       data.coupon_id,
       data.user_id,
       data.order_id,
       date_format(data.used_time,'yyyy-MM-dd'),
       data.used_time,
       date_format(data.used_time,'yyyy-MM-dd') dt
from ods_coupon_use_inc
where dt='2023-01-14' and type='bootstrap-insert' and data.coupon_status='1403';
-- 每日
insert overwrite table dwd_tool_coupon_used_inc partition (dt='2023-01-15')
select data.id,
       data.coupon_id,
       data.user_id,
       data.order_id,
       date_format(data.used_time,'yyyy-MM-dd'),
       data.used_time
from ods_coupon_use_inc
where dt='2023-01-15' and type='update' and data.coupon_status='1403' and array_contains(map_keys(old),'used_time');

-- 7 互动域收藏商品事务事实表
DROP TABLE IF EXISTS dwd_interaction_favor_add_inc;
CREATE EXTERNAL TABLE dwd_interaction_favor_add_inc
(
    `id`          STRING COMMENT '编号',
    `user_id`     STRING COMMENT '用户id',
    `sku_id`      STRING COMMENT 'sku_id',
    `date_id`     STRING COMMENT '日期id',
    `create_time` STRING COMMENT '收藏时间'
) COMMENT '收藏事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_interaction_favor_add_inc/'
TBLPROPERTIES ("orc.compress" = "snappy");
-- 数据装载
-- 首日(动态分区)
set hive.exec.dynamic.partition.mode=nonstrict;
insert overwrite table dwd_interaction_favor_add_inc partition (dt)
select data.id,
       data.user_id,
       data.sku_id,
       date_format(data.create_time,'yyyy-MM-dd'),
       data.create_time,
       date_format(data.create_time,'yyyy-MM-dd') dt
from ods_favor_info_inc
where dt='2023-01-14' and type='bootstrap-insert';
-- 每日
insert overwrite table dwd_interaction_favor_add_inc partition (dt='2023-01-15')
select data.id,
       data.user_id,
       data.sku_id,
       date_format(data.create_time,'yyyy-MM-dd'),
       data.create_time
from ods_favor_info_inc
where dt='2023-01-15' and type='insert';

-- 8 流量域页面浏览事务事实表
DROP TABLE IF EXISTS dwd_traffic_page_view_inc;
CREATE EXTERNAL TABLE dwd_traffic_page_view_inc
(
    `province_id`    STRING COMMENT '省份id',
    `brand`          STRING COMMENT '手机品牌',
    `channel`        STRING COMMENT '渠道',
    `is_new`         STRING COMMENT '是否首次启动',
    `model`          STRING COMMENT '手机型号',
    `mid_id`         STRING COMMENT '设备id',
    `operate_system` STRING COMMENT '操作系统',
    `user_id`        STRING COMMENT '会员id',
    `version_code`   STRING COMMENT 'app版本号',
    `page_item`      STRING COMMENT '目标id ',
    `page_item_type` STRING COMMENT '目标类型',
    `last_page_id`   STRING COMMENT '上页类型',
    `page_id`        STRING COMMENT '页面ID ',
    `source_type`    STRING COMMENT '来源类型',
    `date_id`        STRING COMMENT '日期id',
    `view_time`      STRING COMMENT '跳入时间',
    `session_id`     STRING COMMENT '所属会话id',
    `during_time`    BIGINT COMMENT '持续时间毫秒'
) COMMENT '页面日志表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_traffic_page_view_inc'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日/每日
/*
    session_id说明：
        判断标准：同一个mid中，连续访问页面的数据为同一个会话
        如何划分：
            1.last_page_id为null的数据，用ts设置标记，不为null则标记为nll
            2.按照mid分区，浏览时间排序，开窗，用last_value函数获取窗口中最后一条不为null的标记，即可划分会话

*/
insert overwrite table dwd_traffic_page_view_inc partition (dt='2023-01-14')
select ar,ba,ch,is_new,md,mid,os,uid,vc,item,item_type,last_page_id,page_id,source_type,date_id,view_time,
       concat(mid,'-',last_value(flag,true) over(partition by mid order by view_time)) session_id,
       during_time
from (
         select common.ar,
                common.ba,
                common.ch,
                common.is_new,
                common.md,
                common.mid,
                common.os,
                common.uid,
                common.vc,
                page.item,
                page.item_type,
                page.last_page_id,
                page.page_id,
                page.source_type,
                date_format(from_utc_timestamp(ts, 'Asia/Shanghai'), 'yyyy-MM-dd') date_id,
                from_utc_timestamp(ts, 'Asia/Shanghai')                            view_time,
                page.during_time,
                if(page.last_page_id is null,ts,null) flag
         from ods_log_inc
         where dt = '2023-01-14' and page.during_time is not null
     ) t1;

-- 9 用户域用户注册事务事实表
DROP TABLE IF EXISTS dwd_user_register_inc;
CREATE EXTERNAL TABLE dwd_user_register_inc
(
    `user_id`        STRING COMMENT '用户ID',
    `date_id`        STRING COMMENT '日期ID',
    `create_time`    STRING COMMENT '注册时间',
    `channel`        STRING COMMENT '应用下载渠道',
    `province_id`    STRING COMMENT '省份id',
    `version_code`   STRING COMMENT '应用版本',
    `mid_id`         STRING COMMENT '设备id',
    `brand`          STRING COMMENT '设备品牌',
    `model`          STRING COMMENT '设备型号',
    `operate_system` STRING COMMENT '设备操作系统'
) COMMENT '用户域用户注册事务事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_user_register_inc/'
TBLPROPERTIES ("orc.compress" = "snappy");
-- 数据装载
/*
    数据说明：
        用户的注册信息从业务数据中的来，存在历史数据；而设备等信息从日志数据中来，只有最新的；
        所以，最终的结果是很多用户的设备信息为null~
*/
-- 首日（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
with
ui as (
    select data.id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time
    from ods_user_info_inc
    where dt='2023-01-14' and type='bootstrap-insert'
),pa as (
    select common.uid,
           common.ch,
           common.ar,
           common.vc,
           common.mid,
           common.ba,
           common.md,
           common.os
    from ods_log_inc
    where dt='2023-01-14' and page.page_id is not null and page.page_id='register' and common.uid is not null
),po as (
    select id,area_code
    from ods_base_province_full
    where dt='2023-01-14'
)
insert overwrite table dwd_user_register_inc partition (dt)
select ui.id,
       ui.date_id,
       ui.create_time,
       pa.ch,
       po.id,
       pa.vc,
       pa.mid,
       pa.ba,
       pa.md,
       pa.os,
       ui.date_id dt
from ui
left join pa on ui.id=pa.uid
left join po on pa.ar=po.area_code;
-- 每日
with
ui as (
    select data.id,
           date_format(data.create_time,'yyyy-MM-dd') date_id,
           data.create_time
    from ods_user_info_inc
    where dt='2023-01-15' and type='insert'
),pa as (
    select common.uid,
           common.ch,
           common.ar,
           common.vc,
           common.mid,
           common.ba,
           common.md,
           common.os
    from ods_log_inc
    where dt='2023-01-15' and page.page_id is not null and page.page_id='register' and common.uid is not null
),po as (
    select id,area_code
    from ods_base_province_full
    where dt='2023-01-15'
)
insert overwrite table dwd_user_register_inc partition (dt='2023-01-15')
select ui.id,
       ui.date_id,
       ui.create_time,
       pa.ch,
       po.id,
       pa.vc,
       pa.mid,
       pa.ba,
       pa.md,
       pa.os
from ui
left join pa on ui.id=pa.uid
left join po on pa.ar=po.area_code;

-- 10 用户域用户登录事务事实表
DROP TABLE IF EXISTS dwd_user_login_inc;
CREATE EXTERNAL TABLE dwd_user_login_inc
(
    `user_id`        STRING COMMENT '用户ID',
    `date_id`        STRING COMMENT '日期ID',
    `login_time`     STRING COMMENT '登录时间',
    `channel`        STRING COMMENT '应用下载渠道',
    `province_id`    STRING COMMENT '省份id',
    `version_code`   STRING COMMENT '应用版本',
    `mid_id`         STRING COMMENT '设备id',
    `brand`          STRING COMMENT '设备品牌',
    `model`          STRING COMMENT '设备型号',
    `operate_system` STRING COMMENT '设备操作系统'
) COMMENT '用户域用户登录事务事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dwd/dwd_user_login_inc/'
TBLPROPERTIES ("orc.compress" = "snappy");
-- 数据装载
/*
    数据说明：
        从日志数据读取，没有历史数据，直接存入当日的分区即可

    思路：
        1.划分会话
        2.每个会话中第一条uid不为null的就是登录数据
*/
with
login as (
    select uid,
           date_id,
           view_time,
           ch,
           ar,
           vc,
           ba,
           md,
           os,
           mid
from (
         select uid,
                date_id,
                view_time,
                ch,
                ar,
                vc,
                ba,
                md,
                os,
                mid,
                row_number() over (partition by session_id order by view_time) rn
         from (
                  select uid,
                         date_id,
                         view_time,
                         ch,
                         ar,
                         vc,
                         ba,
                         md,
                         os,
                         mid,
                         concat(mid, '-', last_value(flag, true) over (partition by mid order by ts)) session_id
                  from (
                           select common.uid,
                                  date_format(from_utc_timestamp(ts, 'Asia/Shanghai'), 'yyyy-MM-dd') date_id,
                                  from_utc_timestamp(ts, 'Asia/Shanghai')                            view_time,
                                  common.ch,
                                  common.ar,
                                  common.vc,
                                  common.ba,
                                  common.md,
                                  common.os,
                                  common.mid,
                                  page.last_page_id,
                                  page.page_id,
                                  if(page.last_page_id is null, ts, null)                            flag,
                                  ts
                           from ods_log_inc
                           where dt = '2023-01-14'
                             and page.page_id is not null
                       ) t1
              ) t2
         where uid is not null
     ) t3
where rn=1
),po as (
    select id,area_code
    from ods_base_province_full
    where dt='2023-01-14'
)
insert overwrite table dwd_user_login_inc partition (dt='2023-01-14')
select uid,
       date_id,
       view_time,
       ch,
       po.id,
       vc,
       mid,
       ba,
       md,
       os
from login
join po on login.ar=po.area_code;