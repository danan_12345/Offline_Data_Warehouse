/*
    DIM层建表说明：
        由于DIM层会被频繁的查询，为了提高查询效率，保存文件格式选择列式存储ORC，压缩方式选择速度较快的snappy
*/

-- 1 商品维度表
DROP TABLE IF EXISTS dim_sku_full;
CREATE EXTERNAL TABLE dim_sku_full (
    `id`                   STRING COMMENT 'sku_id',
    `price`                DECIMAL(16, 2) COMMENT '商品价格',
    `sku_name`             STRING COMMENT '商品名称',
    `sku_desc`             STRING COMMENT '商品描述',
    `weight`               DECIMAL(16, 2) COMMENT '重量',
    `is_sale`              BOOLEAN COMMENT '是否在售',
    `spu_id`               STRING COMMENT 'spu编号',
    `spu_name`             STRING COMMENT 'spu名称',
    `category3_id`         STRING COMMENT '三级分类id',
    `category3_name`       STRING COMMENT '三级分类名称',
    `category2_id`         STRING COMMENT '二级分类id',
    `category2_name`       STRING COMMENT '二级分类名称',
    `category1_id`         STRING COMMENT '一级分类id',
    `category1_name`       STRING COMMENT '一级分类名称',
    `tm_id`                STRING COMMENT '品牌id',
    `tm_name`              STRING COMMENT '品牌名称',
    `sku_attr_values`      ARRAY<STRUCT<attr_id :STRING,value_id :STRING,attr_name :STRING,value_name:STRING>> COMMENT '平台属性',
    `sku_sale_attr_values` ARRAY<STRUCT<sale_attr_id :STRING,sale_attr_value_id :STRING,sale_attr_name :STRING,sale_attr_value_name:STRING>> COMMENT '销售属性',
    `create_time`          STRING COMMENT '创建时间'
) COMMENT '商品维度表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dim/dim_sku_full/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
/*
    保证数据幂等性的方式：insert overwrite table ......
*/
with
sku as (
    select id,
           price,
           sku_name,
           sku_desc,
           weight,
           is_sale,
           spu_id,
           tm_id,
           category3_id,
           create_time
    from ods_sku_info_full
    where dt='2023-01-14'
),spu as (
    select id,
           spu_name
    from ods_spu_info_full
    where dt='2023-01-14'
),c3 as (
    select id,
           name,
           category2_id
    from ods_base_category3_full
    where dt='2023-01-14'
),c2 as (
    select id,
           name,
           category1_id
    from ods_base_category2_full
    where dt='2023-01-14'
),c1 as (
    select id,
           name
    from ods_base_category1_full
    where dt='2023-01-14'
),tm as (
    select id,
           tm_name
    from ods_base_trademark_full
    where dt='2023-01-14'
),attr as (
    select sku_id,
           collect_list(named_struct('attr_id',attr_id,'value_id',value_id,'attr_name',attr_name,'value_name',value_name)) attr_values
    from ods_sku_attr_value_full
    where dt='2023-01-14'
    group by sku_id
),sale_attr as (
    select sku_id,
           collect_list(named_struct('sale_attr_id',sale_attr_id,'sale_attr_value_id',sale_attr_value_id,'sale_attr_name',sale_attr_name,'sale_attr_value_name',sale_attr_value_name)) sale_attr_values
    from ods_sku_sale_attr_value_full
    where dt='2023-01-14'
    group by sku_id
)
insert overwrite table dim_sku_full partition (dt='2023-01-14')
select sku.id,
       sku.price,
       sku.sku_name,
       sku.sku_desc,
       sku.weight,
       sku.is_sale,
       sku.spu_id,
       spu.spu_name,
       sku.category3_id,
       c3.name,
       c3.category2_id,
       c2.name,
       c2.category1_id,
       c1.name,
       sku.tm_id,
       tm.tm_name,
       attr.attr_values,
       sale_attr.sale_attr_values,
       sku.create_time
from sku
left join spu on sku.spu_id = spu.id
left join c3 on sku.category3_id = c3.id
left join c2 on c3.category2_id = c2.id
left join c1 on c2.category1_id = c1.id
left join tm on sku.tm_id = tm.id
left join attr on sku.id = attr.sku_id
left join sale_attr on sku.id = sale_attr.sku_id;

-- 2 优惠券维度表
DROP TABLE IF EXISTS dim_coupon_full;
CREATE EXTERNAL TABLE dim_coupon_full (
    `id`               STRING COMMENT '购物券编号',
    `coupon_name`      STRING COMMENT '购物券名称',
    `coupon_type_code` STRING COMMENT '购物券类型编码',
    `coupon_type_name` STRING COMMENT '购物券类型名称',
    `condition_amount` DECIMAL(16, 2) COMMENT '满额数',
    `condition_num`    BIGINT COMMENT '满件数',
    `activity_id`      STRING COMMENT '活动编号',
    `benefit_amount`   DECIMAL(16, 2) COMMENT '减金额',
    `benefit_discount` DECIMAL(16, 2) COMMENT '折扣',
    `benefit_rule`     STRING COMMENT '优惠规则:满元*减*元，满*件打*折',
    `create_time`      STRING COMMENT '创建时间',
    `range_type_code`  STRING COMMENT '优惠范围类型编码',
    `range_type_name`  STRING COMMENT '优惠范围类型名称',
    `limit_num`        BIGINT COMMENT '最多领取次数',
    `taken_count`      BIGINT COMMENT '已领取次数',
    `start_time`       STRING COMMENT '可以领取的开始日期',
    `end_time`         STRING COMMENT '可以领取的结束日期',
    `operate_time`     STRING COMMENT '修改时间',
    `expire_time`      STRING COMMENT '过期时间'
) COMMENT '优惠券维度表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dim/dim_coupon_full/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
with
cou as (
    select id,
           coupon_name,
           coupon_type,
           condition_amount,
           condition_num,
           activity_id,
           benefit_amount,
           benefit_discount,
           case coupon_type
            when '3201' then concat('满',condition_amount,'元减',benefit_amount,'元')
            when '3202' then concat('满',condition_num,'件打',(1 - benefit_discount) * 10,'折')
            when '3203' then concat('满',condition_amount,'元减',benefit_amount,'元')
           end benefit_rule,
           create_time,
           range_type,
           limit_num,
           taken_count,
           start_time,
           end_time,
           operate_time,
           expire_time
    from ods_coupon_info_full
    where dt='2023-01-14'
),dic32 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-14' and parent_code='32'
),dic33 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-14' and parent_code='33'
)
insert overwrite table dim_coupon_full partition (dt='2023-01-14')
select id,
       coupon_name,
       coupon_type,
       dic32.dic_name coupon_type_name,
       condition_amount,
       condition_num,
       activity_id,
       benefit_amount,
       benefit_discount,
       benefit_rule,
       create_time,
       range_type,
       dic33.dic_name range_type_name,
       limit_num,
       taken_count,
       start_time,
       end_time,
       operate_time,
       expire_time
from cou
join dic32 on cou.coupon_type = dic32.dic_code
join dic33 on cou.range_type = dic33.dic_code;

-- 3 活动维度表
DROP TABLE IF EXISTS dim_activity_full;
CREATE EXTERNAL TABLE dim_activity_full (
    `activity_rule_id`   STRING COMMENT '活动规则ID',
    `activity_id`        STRING COMMENT '活动ID',
    `activity_name`      STRING COMMENT '活动名称',
    `activity_type_code` STRING COMMENT '活动类型编码',
    `activity_type_name` STRING COMMENT '活动类型名称',
    `activity_desc`      STRING COMMENT '活动描述',
    `start_time`         STRING COMMENT '开始时间',
    `end_time`           STRING COMMENT '结束时间',
    `create_time`        STRING COMMENT '创建时间',
    `condition_amount`   DECIMAL(16, 2) COMMENT '满减金额',
    `condition_num`      BIGINT COMMENT '满减件数',
    `benefit_amount`     DECIMAL(16, 2) COMMENT '优惠金额',
    `benefit_discount`   DECIMAL(16, 2) COMMENT '优惠折扣',
    `benefit_rule`       STRING COMMENT '优惠规则',
    `benefit_level`      STRING COMMENT '优惠级别'
) COMMENT '活动维度表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dim/dim_activity_full/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
with
ai as (
    select id,
           activity_name,
           activity_type,
           activity_desc,
           start_time,
           end_time,
           create_time
    from ods_activity_info_full
    where dt='2023-01-14'
),ar as (
    select id,
           activity_id,
           condition_amount,
           condition_num,
           benefit_amount,
           benefit_discount,
           case activity_type
            when '3101' then concat('满',condition_amount,'元减',benefit_amount,'元')
            when '3102' then concat('满',condition_num,'件打',(1 - benefit_discount) * 10,'折')
            when '3103' then concat('打',(1 - benefit_discount) * 10,'折')
           end benefit_rule,
           benefit_level
    from ods_activity_rule_full
    where dt='2023-01-14'
),dic31 as (
    select dic_code,dic_name
    from ods_base_dic_full
    where dt='2023-01-14' and parent_code='31'
)
insert overwrite table dim_activity_full partition (dt='2023-01-14')
select ar.id,
       ai.id,
       ai.activity_name,
       ai.activity_type,
       dic31.dic_name,
       ai.activity_desc,
       start_time,
       end_time,
       create_time,
       condition_amount,
       condition_num,
       benefit_amount,
       benefit_discount,
       benefit_rule,
       benefit_level
from ai
join ar on ai.id = ar.activity_id
join dic31 on ai.activity_type = dic31.dic_code;

-- 4 地区维度表
DROP TABLE IF EXISTS dim_province_full;
CREATE EXTERNAL TABLE dim_province_full (
    `id`            STRING COMMENT 'id',
    `province_name` STRING COMMENT '省市名称',
    `area_code`     STRING COMMENT '地区编码',
    `iso_code`      STRING COMMENT '旧版ISO-3166-2编码，供可视化使用',
    `iso_3166_2`    STRING COMMENT '新版IOS-3166-2编码，供可视化使用',
    `region_id`     STRING COMMENT '地区id',
    `region_name`   STRING COMMENT '地区名称'
) COMMENT '地区维度表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dim/dim_province_full/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
with
p as (
    select id,
           name,
           area_code,
           iso_code,
           iso_3166_2,
           region_id
    from ods_base_province_full
    where dt='2023-01-14'
),r as (
    select id,
           region_name
    from ods_base_region_full
    where dt='2023-01-14'
)
insert overwrite table dim_province_full partition (dt='2023-01-14')
select p.id,
       name,
       area_code,
       iso_code,
       iso_3166_2,
       region_id,
       region_name
from p
left join r on p.region_id = r.id;

-- 5 日期维度表
/*
    日期维度表说明：
        1.更新周期长（一年或更久），所以不需要设置分区
        2.日期维度表的数据不是从ODS层来，而是通过外部文件数据导入，所以不能直接经过查询ODS层导入数据；
          需要先创建临时表（\t分割），将外部数据导入临时表，再通过查询临时表将数据导入日期维度表
*/
DROP TABLE IF EXISTS dim_date;
CREATE EXTERNAL TABLE dim_date (
    `date_id`    STRING COMMENT '日期ID',
    `week_id`    STRING COMMENT '周ID,一年中的第几周',
    `week_day`   STRING COMMENT '周几',
    `day`        STRING COMMENT '每月的第几天',
    `month`      STRING COMMENT '一年中的第几月',
    `quarter`    STRING COMMENT '一年中的第几季度',
    `year`       STRING COMMENT '年份',
    `is_workday` STRING COMMENT '是否是工作日',
    `holiday_id` STRING COMMENT '节假日'
) COMMENT '日期维度表'
STORED AS ORC
LOCATION '/warehouse/gmall/dim/dim_date/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 导入数据
-- 1 创建临时表
CREATE TABLE tmp_dim_date (
    `date_id`    STRING COMMENT '日期ID',
    `week_id`    STRING COMMENT '周ID,一年中的第几周',
    `week_day`   STRING COMMENT '周几',
    `day`        STRING COMMENT '每月的第几天',
    `month`      STRING COMMENT '一年中的第几月',
    `quarter`    STRING COMMENT '一年中的第几季度',
    `year`       STRING COMMENT '年份',
    `is_workday` STRING COMMENT '是否是工作日',
    `holiday_id` STRING COMMENT '节假日'
) COMMENT '日期维度表'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/tmp_dim_date/';
-- 导入外部文件数据
load data inpath "/date_info.txt" overwrite into table tmp_dim_date;
-- 2 向日期维度表导入数据
insert overwrite table dim_date
select *
from tmp_dim_date;

-- 6 用户维度表(拉链表)
/*
    用户维度表（拉链表）数据说明：
        1.使用拉链表，可以记录用户信息数据的生命周期（开始日期-结束日期）
        2.维护"9999-99-99"分区，存放用户最新的数据，当日的分区存放在当天过期的用户数据
*/
DROP TABLE IF EXISTS dim_user_zip;
CREATE EXTERNAL TABLE dim_user_zip (
    `id`           STRING COMMENT '用户id',
    `login_name`   STRING COMMENT '用户名称',
    `nick_name`    STRING COMMENT '用户昵称',
    `name`         STRING COMMENT '用户姓名',
    `phone_num`    STRING COMMENT '手机号码',
    `email`        STRING COMMENT '邮箱',
    `user_level`   STRING COMMENT '用户等级',
    `birthday`     STRING COMMENT '生日',
    `gender`       STRING COMMENT '性别',
    `create_time`  STRING COMMENT '创建时间',
    `operate_time` STRING COMMENT '操作时间',
    `start_date`   STRING COMMENT '开始日期',
    `end_date`     STRING COMMENT '结束日期'
) COMMENT '用户维度表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dim/dim_user_zip/'
TBLPROPERTIES ('orc.compress'='snappy');
-- 数据装载
-- 首日装载（所有的用户信息都是最新的，直接存入"9999-99-99"分区）
insert overwrite table dim_user_zip partition (dt='9999-99-99')
select data.id,
       data.login_name,
       data.nick_name,
       data.name,
       data.phone_num,
       data.email,
       data.user_level,
       data.birthday,
       data.gender,
       data.create_time,
       data.operate_time,
       date_format(if(data.operate_time is null,data.create_time,data.operate_time),'yyyy-MM-dd') start_date,
       '9999-99-99' end_date
from ods_user_info_inc
where dt='2023-01-14' and type='bootstrap-insert';
-- 每日数据装载（插入新用户信息 + 更新旧用户信息）
/*
    两种方式：
        方式一：（"9999-99-99"分区数据 + insert数据） left join （update数据），判断update相关字段是否为null，进行取值
        方式二：将所有数据union，按照开始时间进行倒序排列，最新的放到"9999-99-99"分区，过期的就放到当天的分区

        本项目选择：方式二

    动态分区注意：
        1.开启非严格模式 set hive.exec.dynamic.partition.mode=nonstrict;
        2.添加dt字段 insert overwrite table dim_user_zip partition (dt)
*/
set hive.exec.dynamic.partition.mode=nonstrict;
with
old as (
    select id,
           login_name,
           nick_name,
           name,
           phone_num,
           email,
           user_level,
           birthday,
           gender,
           create_time,
           operate_time,
           start_date,
           end_date
    from dim_user_zip
    where dt='9999-99-99'
),i as (
    select data.id,
           data.login_name,
           data.nick_name,
           data.name,
           data.phone_num,
           data.email,
           data.user_level,
           data.birthday,
           data.gender,
           data.create_time,
           data.operate_time,
           date_format(data.create_time,'yyyy-MM-dd') start_date,
           '9999-99-99' end_date
    from ods_user_info_inc
    where dt='2023-01-15' and type='insert'
),u as (
    select data.id,
           data.login_name,
           data.nick_name,
           data.name,
           data.phone_num,
           data.email,
           data.user_level,
           data.birthday,
           data.gender,
           data.create_time,
           data.operate_time,
           date_format(if(data.operate_time is null,data.create_time,data.operate_time),'yyyy-MM-dd') start_date,
           '9999-99-99' end_date
    from ods_user_info_inc
    where dt='2023-01-15' and type='update'
)
insert overwrite table dim_user_zip partition (dt)
select id,
       login_name,
       nick_name,
       name,
       phone_num,
       email,
       user_level,
       birthday,
       gender,
       create_time,
       operate_time,
       start_date,
       if(rn=1,'9999-99-99','2023-01-15') end_date,
       if(rn=1,'9999-99-99','2023-01-15') dt
from (
         select id,
                login_name,
                nick_name,
                name,
                phone_num,
                email,
                user_level,
                birthday,
                gender,
                create_time,
                operate_time,
                start_date,
                end_date,
                row_number() over (partition by id order by start_date desc) rn
         from (
                  select *
                  from old
                  union all
                  select *
                  from i
                  union all
                  select *
                  from u
              ) t1
) t2;

