-- *******************************************************************************************
--                                          最近1日汇总表
-- *******************************************************************************************

-- 1 交易域用户商品粒度订单最近一日汇总表
DROP TABLE IF EXISTS dws_trade_user_sku_order_1d;
CREATE EXTERNAL TABLE dws_trade_user_sku_order_1d
(
    `user_id`                   STRING COMMENT '用户id',
    `sku_id`                    STRING COMMENT 'sku_id',
    `sku_name`                  STRING COMMENT 'sku名称',
    `category1_id`              STRING COMMENT '一级分类id',
    `category1_name`            STRING COMMENT '一级分类名称',
    `category2_id`              STRING COMMENT '一级分类id',
    `category2_name`            STRING COMMENT '一级分类名称',
    `category3_id`              STRING COMMENT '一级分类id',
    `category3_name`            STRING COMMENT '一级分类名称',
    `tm_id`                     STRING COMMENT '品牌id',
    `tm_name`                   STRING COMMENT '品牌名称',
    `order_count_1d`            BIGINT COMMENT '最近1日下单次数',
    `order_num_1d`              BIGINT COMMENT '最近1日下单件数',
    `order_original_amount_1d`  DECIMAL(16, 2) COMMENT '最近1日下单原始金额',
    `activity_reduce_amount_1d` DECIMAL(16, 2) COMMENT '最近1日活动优惠金额',
    `coupon_reduce_amount_1d`   DECIMAL(16, 2) COMMENT '最近1日优惠券优惠金额',
    `order_total_amount_1d`     DECIMAL(16, 2) COMMENT '最近1日下单最终金额'
) COMMENT '交易域用户商品粒度订单最近1日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_user_sku_order_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
with
od as (
    select dt,user_id,sku_id,
           count(distinct order_id) order_count_1d,
           sum(sku_num) order_num_1d,
           sum(split_original_amount) order_original_amount_1d,
           sum(split_activity_amount) activity_reduce_amount_1d,
           sum(split_coupon_amount) coupon_reduce_amount_1d,
           sum(split_total_amount) order_total_amount_1d
    from dwd_trade_order_detail_inc
    where dt<='2023-01-14'
    group by dt,user_id,sku_id
),si as (
    select id,
           price,
           sku_name,
           sku_desc,
           weight,
           is_sale,
           spu_id,
           spu_name,
           category3_id,
           category3_name,
           category2_id,
           category2_name,
           category1_id,
           category1_name,
           tm_id,
           tm_name
    from dim_sku_full
    where dt='2023-01-14'
)
insert overwrite table dws_trade_user_sku_order_1d partition (dt)
select od.user_id,
       od.sku_id,
       si.sku_name,
       si.category1_id,
       si.category1_name,
       si.category2_id,
       si.category2_name,
       si.category3_id,
       si.category3_name,
       si.tm_id,
       si.tm_name,
       od.order_count_1d,
       od.order_num_1d,
       od.order_original_amount_1d,
       nvl(od.activity_reduce_amount_1d,0.0),
       nvl(od.coupon_reduce_amount_1d,0.0),
       od.order_total_amount_1d,
       od.dt
from od
join si on od.sku_id=si.id;
-- 每日
with
od as (
    select dt,user_id,sku_id,
           count(distinct order_id) order_count_1d,
           sum(sku_num) order_num_1d,
           sum(split_original_amount) order_original_amount_1d,
           sum(split_activity_amount) activity_reduce_amount_1d,
           sum(split_coupon_amount) coupon_reduce_amount_1d,
           sum(split_total_amount) order_total_amount_1d
    from dwd_trade_order_detail_inc
    where dt='2023-01-15'
    group by dt,user_id,sku_id
),si as (
    select id,
           price,
           sku_name,
           sku_desc,
           weight,
           is_sale,
           spu_id,
           spu_name,
           category3_id,
           category3_name,
           category2_id,
           category2_name,
           category1_id,
           category1_name,
           tm_id,
           tm_name
    from dim_sku_full
    where dt='2023-01-15'
)
insert overwrite table dws_trade_user_sku_order_1d partition (dt='2023-01-15')
select od.user_id,
       od.sku_id,
       si.sku_name,
       si.category1_id,
       si.category1_name,
       si.category2_id,
       si.category2_name,
       si.category3_id,
       si.category3_name,
       si.tm_id,
       si.tm_name,
       od.order_count_1d,
       od.order_num_1d,
       od.order_original_amount_1d,
       nvl(od.activity_reduce_amount_1d,0.0),
       nvl(od.coupon_reduce_amount_1d,0.0),
       od.order_total_amount_1d
from od
join si on od.sku_id=si.id;

-- 2 交易域用户粒度订单最近1日汇总表
DROP TABLE IF EXISTS dws_trade_user_order_1d;
CREATE EXTERNAL TABLE dws_trade_user_order_1d
(
    `user_id`                   STRING COMMENT '用户id',
    `order_count_1d`            BIGINT COMMENT '最近1日下单次数',
    `order_num_1d`              BIGINT COMMENT '最近1日下单商品件数',
    `order_original_amount_1d`  DECIMAL(16, 2) COMMENT '最近1日最近1日下单原始金额',
    `activity_reduce_amount_1d` DECIMAL(16, 2) COMMENT '最近1日下单活动优惠金额',
    `coupon_reduce_amount_1d`   DECIMAL(16, 2) COMMENT '下单优惠券优惠金额',
    `order_total_amount_1d`     DECIMAL(16, 2) COMMENT '最近1日下单最终金额'
) COMMENT '交易域用户粒度订单最近1日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_user_order_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
insert overwrite table dws_trade_user_order_1d partition (dt)
select user_id,
       count(distinct order_id),
       sum(sku_num),
       sum(split_original_amount),
       nvl(sum(split_activity_amount),0.0),
       nvl(sum(split_coupon_amount),0.0),
       sum(split_total_amount),
       dt
from dwd_trade_order_detail_inc
where dt<='2023-01-14'
group by dt,user_id;
-- 每日
insert overwrite table dws_trade_user_order_1d partition (dt='2023-01-15')
select user_id,
       count(distinct order_id),
       sum(sku_num),
       sum(split_original_amount),
       nvl(sum(split_activity_amount),0.0),
       nvl(sum(split_coupon_amount),0.0),
       sum(split_total_amount)
from dwd_trade_order_detail_inc
where dt='2023-01-15'
group by user_id;

-- 3 交易域用户粒度加购最近1日汇总表
DROP TABLE IF EXISTS dws_trade_user_cart_add_1d;
CREATE EXTERNAL TABLE dws_trade_user_cart_add_1d
(
    `user_id`           STRING COMMENT '用户id',
    `cart_add_count_1d` BIGINT COMMENT '最近1日加购次数',
    `cart_add_num_1d`   BIGINT COMMENT '最近1日加购商品件数'
) COMMENT '交易域用户粒度加购最近1日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_user_cart_add_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
insert overwrite table dws_trade_user_cart_add_1d partition (dt)
select user_id,
       count(1),
       sum(sku_num),
       dt
from dwd_trade_cart_add_inc
where dt<='2023-01-14'
group by dt,user_id;
-- 每日
insert overwrite table dws_trade_user_cart_add_1d partition (dt='2023-01-15')
select user_id,
       count(1),
       sum(sku_num)
from dwd_trade_cart_add_inc
where dt='2023-01-15'
group by user_id;

-- 4 交易域用户粒度支付最近1日汇总表
DROP TABLE IF EXISTS dws_trade_user_payment_1d;
CREATE EXTERNAL TABLE dws_trade_user_payment_1d
(
    `user_id`           STRING COMMENT '用户id',
    `payment_count_1d`  BIGINT COMMENT '最近1日支付次数',
    `payment_num_1d`    BIGINT COMMENT '最近1日支付商品件数',
    `payment_amount_1d` DECIMAL(16, 2) COMMENT '最近1日支付金额'
) COMMENT '交易域用户粒度支付最近1日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_user_payment_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
insert overwrite table dws_trade_user_payment_1d partition (dt)
select user_id,
       count(1),
       sum(sku_num),
       sum(split_payment_amount),
       dt
from dwd_trade_pay_detail_suc_inc
where dt<='2023-01-14'
group by dt,user_id;
-- 每日
insert overwrite table dws_trade_user_payment_1d partition (dt='2023-01-15')
select user_id,
       count(1),
       sum(sku_num),
       sum(split_payment_amount)
from dwd_trade_pay_detail_suc_inc
where dt='2023-01-15'
group by user_id;

-- 5 交易域省份粒度订单最近1日汇总表
DROP TABLE IF EXISTS dws_trade_province_order_1d;
CREATE EXTERNAL TABLE dws_trade_province_order_1d
(
    `province_id`               STRING COMMENT '用户id',
    `province_name`             STRING COMMENT '省份名称',
    `area_code`                 STRING COMMENT '地区编码',
    `iso_code`                  STRING COMMENT '旧版ISO-3166-2编码',
    `iso_3166_2`                STRING COMMENT '新版版ISO-3166-2编码',
    `order_count_1d`            BIGINT COMMENT '最近1日下单次数',
    `order_original_amount_1d`  DECIMAL(16, 2) COMMENT '最近1日下单原始金额',
    `activity_reduce_amount_1d` DECIMAL(16, 2) COMMENT '最近1日下单活动优惠金额',
    `coupon_reduce_amount_1d`   DECIMAL(16, 2) COMMENT '最近1日下单优惠券优惠金额',
    `order_total_amount_1d`     DECIMAL(16, 2) COMMENT '最近1日下单最终金额'
) COMMENT '交易域省份粒度订单最近1日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_province_order_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
with
od as (
    select dt,user_id,province_id,
           count(distinct order_id) order_count_1d,
           sum(split_original_amount) order_original_amount_1d,
           nvl(sum(split_activity_amount),0.0) activity_reduce_amount_1d,
           nvl(sum(split_coupon_amount),0.0)  coupon_reduce_amount_1d,
           sum(split_total_amount) order_total_amount_1d
    from dwd_trade_order_detail_inc
    where dt<='2023-01-14'
    group by dt,user_id,province_id
),pr as (
    select id,province_name,area_code,iso_code,iso_3166_2
    from dim_province_full
    where dt='2023-01-14'
)
insert overwrite table dws_trade_province_order_1d partition (dt)
select od.province_id,
       pr.province_name,
       pr.area_code,
       pr.iso_code,
       pr.iso_3166_2,
       od.order_count_1d,
       od.order_original_amount_1d,
       od.activity_reduce_amount_1d,
       od.coupon_reduce_amount_1d,
       od.order_total_amount_1d,
       dt
from od
join pr on od.province_id=pr.id;
-- 每日
with
od as (
    select user_id,province_id,
           count(distinct order_id) order_count_1d,
           sum(split_original_amount) order_original_amount_1d,
           nvl(sum(split_activity_amount),0.0) activity_reduce_amount_1d,
           nvl(sum(split_coupon_amount),0.0)  coupon_reduce_amount_1d,
           sum(split_total_amount) order_total_amount_1d
    from dwd_trade_order_detail_inc
    where dt='2023-01-15'
    group by user_id,province_id
),pr as (
    select id,province_name,area_code,iso_code,iso_3166_2
    from dim_province_full
    where dt='2023-01-15'
)
insert overwrite table dws_trade_province_order_1d partition (dt='2023-01-15')
select od.province_id,
       pr.province_name,
       pr.area_code,
       pr.iso_code,
       pr.iso_3166_2,
       od.order_count_1d,
       od.order_original_amount_1d,
       od.activity_reduce_amount_1d,
       od.coupon_reduce_amount_1d,
       od.order_total_amount_1d
from od
join pr on od.province_id=pr.id;

-- 6 工具域用户优惠券粒度优惠券使用（支付）最近1日汇总表
DROP TABLE IF EXISTS dws_tool_user_coupon_coupon_used_1d;
CREATE EXTERNAL TABLE dws_tool_user_coupon_coupon_used_1d
(
    `user_id`          STRING COMMENT '用户id',
    `coupon_id`        STRING COMMENT '优惠券id',
    `coupon_name`      STRING COMMENT '优惠券名称',
    `coupon_type_code` STRING COMMENT '优惠券类型编码',
    `coupon_type_name` STRING COMMENT '优惠券类型名称',
    `benefit_rule`     STRING COMMENT '优惠规则',
    `used_count_1d`    STRING COMMENT '使用(支付)次数'
) COMMENT '工具域用户优惠券粒度优惠券使用(支付)最近1日汇总表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_tool_user_coupon_coupon_used_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日数据（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
with
cu as (
    select dt,user_id,coupon_id,
           count(1) used_count_1d
    from dwd_tool_coupon_used_inc
    where dt<='2023-01-14'
    group by dt,user_id,coupon_id
),ci as (
    select id,coupon_name,coupon_type_code,coupon_type_name,benefit_rule
    from dim_coupon_full
    where dt='2023-01-14'
)
insert overwrite table dws_tool_user_coupon_coupon_used_1d partition (dt)
select cu.user_id,
       cu.coupon_id,
       ci.coupon_name,
       ci.coupon_type_code,
       ci.coupon_type_name,
       ci.benefit_rule,
       cu.used_count_1d,
       dt
from cu
join ci on cu.coupon_id=ci.id;
-- 每日
with
cu as (
    select user_id,coupon_id,
           count(1) used_count_1d
    from dwd_tool_coupon_used_inc
    where dt='2023-01-15'
    group by user_id,coupon_id
),ci as (
    select id,coupon_name,coupon_type_code,coupon_type_name,benefit_rule
    from dim_coupon_full
    where dt='2023-01-15'
)
insert overwrite table dws_tool_user_coupon_coupon_used_1d partition (dt='2023-01-15')
select cu.user_id,
       cu.coupon_id,
       ci.coupon_name,
       ci.coupon_type_code,
       ci.coupon_type_name,
       ci.benefit_rule,
       cu.used_count_1d
from cu
join ci on cu.coupon_id=ci.id;

-- 7 互动域商品粒度收藏商品最近1日汇总表
DROP TABLE IF EXISTS dws_interaction_sku_favor_add_1d;
CREATE EXTERNAL TABLE dws_interaction_sku_favor_add_1d
(
    `sku_id`             STRING COMMENT 'sku id',
    `sku_name`           STRING COMMENT 'sku 名称',
    `category1_id`       STRING COMMENT '一级分类id',
    `category1_name`     STRING COMMENT '一级分类名称',
    `category2_id`       STRING COMMENT '一级分类id',
    `category2_name`     STRING COMMENT '一级分类名称',
    `category3_id`       STRING COMMENT '一级分类id',
    `category3_name`     STRING COMMENT '一级分类名称',
    `tm_id`              STRING COMMENT '品牌id',
    `tm_name`            STRING COMMENT '品牌名称',
    `favor_add_count_1d` BIGINT COMMENT '商品被收藏次数'
) COMMENT '互动域商品粒度收藏商品最近1日汇总表'
    PARTITIONED BY (`dt` STRING)
    STORED AS ORC
    LOCATION '/warehouse/gmall/dws/dws_interaction_sku_favor_add_1d'
    TBLPROPERTIES ('orc.compress' = 'snappy');
-- 首日装载（动态分区）
set hive.exec.dynamic.partition.mode=nonstrict;
with
fa as (
    select dt,sku_id,
           count(1) favor_add_count_1d
    from dwd_interaction_favor_add_inc
    where dt<='2023-01-14'
    group by dt,sku_id
),si as (
    select id,
           price,
           sku_name,
           sku_desc,
           weight,
           is_sale,
           spu_id,
           spu_name,
           category3_id,
           category3_name,
           category2_id,
           category2_name,
           category1_id,
           category1_name,
           tm_id,
           tm_name
    from dim_sku_full
    where dt='2023-01-14'
)
insert overwrite table dws_interaction_sku_favor_add_1d partition (dt)
select fa.sku_id,
       si.sku_name,
       si.category1_id,
       si.category1_name,
       si.category2_id,
       si.category2_name,
       si.category3_id,
       si.category3_name,
       si.tm_id,
       si.tm_name,
       fa.favor_add_count_1d,
       dt
from fa
join si on fa.sku_id=si.id;
-- 每日装载
with
fa as (
    select sku_id,
           count(1) favor_add_count_1d
    from dwd_interaction_favor_add_inc
    where dt='2023-01-15'
    group by sku_id
),si as (
    select id,
           price,
           sku_name,
           sku_desc,
           weight,
           is_sale,
           spu_id,
           spu_name,
           category3_id,
           category3_name,
           category2_id,
           category2_name,
           category1_id,
           category1_name,
           tm_id,
           tm_name
    from dim_sku_full
    where dt='2023-01-15'
)
insert overwrite table dws_interaction_sku_favor_add_1d partition (dt='2023-01-15')
select fa.sku_id,
       si.sku_name,
       si.category1_id,
       si.category1_name,
       si.category2_id,
       si.category2_name,
       si.category3_id,
       si.category3_name,
       si.tm_id,
       si.tm_name,
       fa.favor_add_count_1d
from fa
join si on fa.sku_id=si.id;

-- 8 流量域会话粒度页面浏览最近一日汇总表
DROP TABLE IF EXISTS dws_traffic_session_page_view_1d;
CREATE EXTERNAL TABLE dws_traffic_session_page_view_1d
(
    `session_id`     STRING COMMENT '会话id',
    `mid_id`         string comment '设备id',
    `brand`          string comment '手机品牌',
    `model`          string comment '手机型号',
    `operate_system` string comment '操作系统',
    `version_code`   string comment 'app版本号',
    `channel`        string comment '渠道',
    `during_time_1d` BIGINT COMMENT '最近1日访问时长',
    `page_count_1d`  BIGINT COMMENT '最近1日访问页面数'
) COMMENT '流量域会话粒度页面浏览最近1日汇总表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_traffic_session_page_view_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日/每日
insert overwrite table dws_traffic_session_page_view_1d partition (dt='2023-01-14')
select session_id,
       mid_id,
       brand,
       model,
       operate_system,
       version_code,
       channel,
       sum(during_time),
       count(1)
from dwd_traffic_page_view_inc
where dt='2023-01-14'
group by session_id,mid_id,brand,model,operate_system,version_code,channel;

-- 9 流量域访客页面粒度页面浏览1日汇总表
DROP TABLE IF EXISTS dws_traffic_page_visitor_page_view_1d;
CREATE EXTERNAL TABLE dws_traffic_page_visitor_page_view_1d
(
    `mid_id`         STRING COMMENT '访客id',
    `brand`          string comment '手机品牌',
    `model`          string comment '手机型号',
    `operate_system` string comment '操作系统',
    `page_id`        STRING COMMENT '页面id',
    `during_time_1d` BIGINT COMMENT '最近1日浏览时长',
    `view_count_1d`  BIGINT COMMENT '最近1日访问次数'
) COMMENT '流量域访客页面粒度页面浏览最近1日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_traffic_page_visitor_page_view_1d'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日/每日
insert overwrite table dws_traffic_page_visitor_page_view_1d partition (dt='2023-01-14')
select mid_id,brand,model,operate_system,page_id,
       sum(during_time),
       count(1)
from dwd_traffic_page_view_inc
where dt='2023-01-14'
group by mid_id,brand,model,operate_system,page_id;

-- *******************************************************************************************
--                                          最近n日汇总表
-- *******************************************************************************************

-- 1 交易域用户商品粒度订单最近n日汇总表
DROP TABLE IF EXISTS dws_trade_user_sku_order_nd;
CREATE EXTERNAL TABLE dws_trade_user_sku_order_nd
(
    `user_id`                    STRING COMMENT '用户id',
    `sku_id`                     STRING COMMENT 'sku_id',
    `sku_name`                   STRING COMMENT 'sku名称',
    `category1_id`               STRING COMMENT '一级分类id',
    `category1_name`             STRING COMMENT '一级分类名称',
    `category2_id`               STRING COMMENT '一级分类id',
    `category2_name`             STRING COMMENT '一级分类名称',
    `category3_id`               STRING COMMENT '一级分类id',
    `category3_name`             STRING COMMENT '一级分类名称',
    `tm_id`                      STRING COMMENT '品牌id',
    `tm_name`                    STRING COMMENT '品牌名称',
    `recent_days`                BIGINT COMMENT '最近天数',
    `order_count_nd`             STRING COMMENT '最近n日下单次数',
    `order_num_nd`               BIGINT COMMENT '最近n日下单件数',
    `order_original_amount_nd`   DECIMAL(16, 2) COMMENT '最近n日下单原始金额',
    `activity_reduce_amount_nd`  DECIMAL(16, 2) COMMENT '最近n日活动优惠金额',
    `coupon_reduce_amount_nd`    DECIMAL(16, 2) COMMENT '最近n日优惠券优惠金额',
    `order_total_amount_nd`      DECIMAL(16, 2) COMMENT '最近n日下单最终金额'
) COMMENT '交易域用户商品粒度订单最近n日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_user_sku_order_nd'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日/每日
insert overwrite table dws_trade_user_sku_order_nd partition (dt='2023-01-14')
select user_id,sku_id,sku_name,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name,tm_id,tm_name,recent_days,
       sum(order_count_1d) flag,
       sum(order_num_1d),
       sum(order_original_amount_1d),
       sum(activity_reduce_amount_1d),
       sum(coupon_reduce_amount_1d),
       sum(order_total_amount_1d)
from dws_trade_user_sku_order_1d
lateral view explode(array(1,7,30)) tmp as recent_days
where dt<='2023-01-14' and dt>=date_sub('2023-01-14',recent_days - 1)
group by user_id,sku_id,sku_name,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name,tm_id,tm_name,recent_days;

-- 2 交易域省份粒度订单最近n日汇总表
DROP TABLE IF EXISTS dws_trade_province_order_nd;
CREATE EXTERNAL TABLE dws_trade_province_order_nd
(
    `province_id`                STRING COMMENT '用户id',
    `province_name`              STRING COMMENT '省份名称',
    `area_code`                  STRING COMMENT '地区编码',
    `iso_code`                   STRING COMMENT '旧版ISO-3166-2编码',
    `iso_3166_2`                 STRING COMMENT '新版版ISO-3166-2编码',
    `recent_days`                BIGINT COMMENT '最近天数',
    `order_count_nd`             BIGINT COMMENT '最近n日下单次数',
    `order_original_amount_nd`   DECIMAL(16, 2) COMMENT '最近n日下单原始金额',
    `activity_reduce_amount_nd`  DECIMAL(16, 2) COMMENT '最近n日下单活动优惠金额',
    `coupon_reduce_amount_nd`    DECIMAL(16, 2) COMMENT '最近n日下单优惠券优惠金额',
    `order_total_amount_nd`      DECIMAL(16, 2) COMMENT '最近n日下单最终金额'
) COMMENT '交易域省份粒度订单最近n日汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_province_order_nd'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日/每日
insert overwrite table dws_trade_province_order_nd partition (dt='2023-01-14')
select province_id,province_name,area_code,iso_code,iso_3166_2,recent_days,
       sum(order_count_1d),
       sum(order_original_amount_1d),
       sum(activity_reduce_amount_1d),
       sum(coupon_reduce_amount_1d),
       sum(order_total_amount_1d)
from dws_trade_province_order_1d
lateral view explode(array(1,7,30)) tmp as recent_days
where dt<='2023-01-14' and dt>=date_sub('2023-01-14',recent_days - 1)
group by province_id,province_name,area_code,iso_code,iso_3166_2,recent_days;

-- *******************************************************************************************
--                                          历史至今汇总表
-- *******************************************************************************************

/*
    区分首日，每日数据装载的原因：
        如果每天都查询历史所有数据，随着数据量的增大，查询的效率越来越低。
        为了解决以上问题，只有首日查询历史全量数据，后续每日在前一天的历史数据汇总的基础上，计算的到当天的结果
*/

-- 1 交易域用户粒度订单历史至今汇总表
DROP TABLE IF EXISTS dws_trade_user_order_td;
CREATE EXTERNAL TABLE dws_trade_user_order_td
(
    `user_id`                   STRING COMMENT '用户id',
    `order_date_first`          STRING COMMENT '首次下单日期',
    `order_date_last`           STRING COMMENT '末次下单日期',
    `order_count_td`            BIGINT COMMENT '下单次数',
    `order_num_td`              BIGINT COMMENT '购买商品件数',
    `original_amount_td`        DECIMAL(16, 2) COMMENT '原始金额',
    `activity_reduce_amount_td` DECIMAL(16, 2) COMMENT '活动优惠金额',
    `coupon_reduce_amount_td`   DECIMAL(16, 2) COMMENT '优惠券优惠金额',
    `total_amount_td`           DECIMAL(16, 2) COMMENT '最终金额'
) COMMENT '交易域用户粒度订单历史至今汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_trade_user_order_td'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日
insert overwrite table dws_trade_user_order_td partition (dt='2023-01-14')
select user_id,
       min(dt),
       max(dt),
       sum(order_count_1d),
       sum(order_num_1d),
       sum(order_original_amount_1d),
       sum(activity_reduce_amount_1d),
       sum(coupon_reduce_amount_1d),
       sum(order_total_amount_1d)
from dws_trade_user_order_1d
where dt<='2023-01-14'
group by user_id;
-- 每日
insert overwrite table dws_trade_user_order_td partition (dt='2023-01-15')
select user_id,
       min(order_date_first),
       max(order_date_last),
       sum(order_count_td),
       sum(order_num_td),
       sum(original_amount_td),
       sum(activity_reduce_amount_td),
       sum(coupon_reduce_amount_td),
       sum(total_amount_td)
from (
         (
             select user_id,
                    order_date_first,
                    order_date_last,
                    order_count_td,
                    order_num_td,
                    original_amount_td,
                    activity_reduce_amount_td,
                    coupon_reduce_amount_td,
                    total_amount_td
             from dws_trade_user_order_td
             where dt = date_sub('2023-01-15', 1)
         )
         union all
         (select user_id,
                 min(dt),
                 max(dt),
                 sum(order_count_1d),
                 sum(order_num_1d),
                 sum(order_original_amount_1d),
                 sum(activity_reduce_amount_1d),
                 sum(coupon_reduce_amount_1d),
                 sum(order_total_amount_1d)
          from dws_trade_user_order_1d
          where dt = '2023-01-15'
          group by user_id)
     ) t1
group by user_id;

-- 2 用户域用户粒度登录历史至今汇总表
DROP TABLE IF EXISTS dws_user_user_login_td;
CREATE EXTERNAL TABLE dws_user_user_login_td
(
    `user_id`         STRING COMMENT '用户id',
    `login_date_last` STRING COMMENT '末次登录日期',
    `login_count_td`  BIGINT COMMENT '累计登录次数'
) COMMENT '用户域用户粒度登录历史至今汇总事实表'
PARTITIONED BY (`dt` STRING)
STORED AS ORC
LOCATION '/warehouse/gmall/dws/dws_user_user_login_td'
TBLPROPERTIES ('orc.compress' = 'snappy');
-- 数据装载
-- 首日
with
ui as (
    select id,create_time
    from dim_user_zip
    where dt='9999-99-99'
),login as (
    select user_id,
           max(date_id) last_login,
           count(1) login_count
    from dwd_user_login_inc
    where dt='2023-01-14'
    group by user_id
)
insert overwrite table dws_user_user_login_td partition (dt='2023-01-14')
select ui.id,
       nvl(login.last_login,date_format(create_time,'yyyy-MM-dd')),
       nvl(login.login_count,1)
from ui
left join login on ui.id=login.user_id;
-- 每日
insert overwrite table dws_user_user_login_td partition (dt='2023-01-15')
select user_id,
       max(login_date_last),
       sum(login_count_td)
from (
         (select user_id, login_date_last, login_count_td
          from dws_user_user_login_td
          where dt = date_sub('2023-01-15', 1)
         )
         union all
         (
             select user_id,
                    max(date_id) last_login,
                    count(1)     login_count
             from dwd_user_login_inc
             where dt = '2023-01-15'
             group by user_id
         )
     ) t1
group by user_id;
