/*
    ADS层建表说明：
        1.不需要分区，数据量不大，避免小文件问题
        2.采用默认的textfile存储格式，数据量小，且易于格式化
        3.不需要压缩，数据量不大
    导入数据如何保证幂等性？
        insert overwrite table xxx
        select * from xxx where dt != 某天
        union all
        select * from ...某天的查询结果
*/

-- *******************************************************************************************
--                                          流量主题
-- *******************************************************************************************
-- 1 各渠道流量统计
DROP TABLE IF EXISTS ads_traffic_stats_by_channel;
CREATE EXTERNAL TABLE ads_traffic_stats_by_channel (
    `dt`               STRING COMMENT '统计日期',
    `recent_days`      BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `channel`          STRING COMMENT '渠道',
    `uv_count`         BIGINT COMMENT '访客人数',
    `avg_duration_sec` BIGINT COMMENT '会话平均停留时长，单位为秒',
    `avg_page_count`   BIGINT COMMENT '会话平均浏览页面数',
    `sv_count`         BIGINT COMMENT '会话数',
    `bounce_rate`      DECIMAL(16, 2) COMMENT '跳出率'
) COMMENT '各渠道流量统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_traffic_stats_by_channel/';
-- 数据装载
insert overwrite table ads_traffic_stats_by_channel
select * from ads_traffic_stats_by_channel where dt!='2023-01-14'
union all
select '2023-01-14' dt,recent_days,channel,
       count(distinct mid_id),
       avg(during_time_1d) / 1000,
       avg(page_count_1d),
       count(1),
       sum(if(page_count_1d=1,1,0)) / count(1)
from dws_traffic_session_page_view_1d
lateral view explode(array(1,7,30)) tmp as recent_days
where dt <='2023-01-14' and dt>=date_sub('2023-01-14',recent_days - 1)
group by recent_days,channel;

-- 2 路径分析(桑基图)
DROP TABLE IF EXISTS ads_page_path;
CREATE EXTERNAL TABLE ads_page_path
(
    `dt`          STRING COMMENT '统计日期',
    `source`      STRING COMMENT '跳转起始页面ID',
    `target`      STRING COMMENT '跳转终到页面ID',
    `path_count`  BIGINT COMMENT '跳转次数'
) COMMENT '页面浏览路径分析'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_page_path/';
-- 数据装载
insert overwrite table ads_page_path
select * from ads_page_path where dt!='2023-01-14'
union all
select '2023-01-14' dt,
       source,
       target,
       count(1)
from (
         select page_id source, lead(page_id, 1, 'null') over (partition by session_id order by view_time) target
         from dwd_traffic_page_view_inc
         where dt = '2023-01-14'
     ) t1
group by source,target;

-- *******************************************************************************************
--                                          用户主题
-- *******************************************************************************************
-- 1 用户变动统计
DROP TABLE IF EXISTS ads_user_change;
CREATE EXTERNAL TABLE ads_user_change
(
    `dt`               STRING COMMENT '统计日期',
    `user_churn_count` BIGINT COMMENT '流失用户数',
    `user_back_count`  BIGINT COMMENT '回流用户数'
) COMMENT '用户变动统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_user_change/';
-- 数据装载
insert overwrite table ads_user_change
select * from ads_user_change where dt!='2023-01-14'
union all
select '2023-01-14' dt,sum(falg1),sum(flag2)
from (
         select if(datediff('2023-01-14', last_login) = 7, 1, 0) falg1,
                if(last_login = '2023-01-14' and datediff('2023-01-14', last2_login) = 8, 1, 0) flag2
         from (
                  select max(login_date_last) last_login,
                         min(login_date_last) last2_login
                  from dws_user_user_login_td
                  where dt <= '2023-01-14' and dt >= date_sub('2023-01-14', 1)
                  group by user_id
              ) t1
     ) t2;

-- 2 用户留存率
DROP TABLE IF EXISTS ads_user_retention;
CREATE EXTERNAL TABLE ads_user_retention
(
    `dt`              STRING COMMENT '统计日期',
    `create_date`     STRING COMMENT '用户新增日期',
    `retention_day`   INT COMMENT '截至当前日期留存天数',
    `retention_count` BIGINT COMMENT '留存用户数量',
    `new_user_count`  BIGINT COMMENT '新增用户数量',
    `retention_rate`  DECIMAL(16, 2) COMMENT '留存率'
) COMMENT '用户留存率'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_user_retention/';
-- 数据装载
with
register as (
    select dt,user_id
    from dwd_user_register_inc
    where dt<'2023-01-14' and dt>=date_sub('2023-01-14',7)
),login as (
    select user_id,login_date_last
    from dws_user_user_login_td
    where dt='2023-01-14'
)
insert overwrite table ads_user_retention
select * from ads_user_retention where dt!='2023-01-14'
union all
select '2023-01-14' dt,
       dt create_date,
       datediff('2023-01-14',dt),
       sum(if(login_date_last='2023-01-14',1,0)),
       count(distinct register.user_id),
       sum(if(login_date_last='2023-01-14',1,0)) / count(distinct register.user_id)
from register join login on register.user_id=login.user_id
group by dt;

-- 3 用户新增活跃统计
DROP TABLE IF EXISTS ads_user_stats;
CREATE EXTERNAL TABLE ads_user_stats
(
    `dt`                STRING COMMENT '统计日期',
    `recent_days`       BIGINT COMMENT '最近n日,1:最近1日,7:最近7日,30:最近30日',
    `new_user_count`    BIGINT COMMENT '新增用户数',
    `active_user_count` BIGINT COMMENT '活跃用户数'
) COMMENT '用户新增活跃统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_user_stats/';
-- 数据装载
with
new as (
    select recent_days,count(distinct user_id) new_count
    from dwd_user_register_inc
    lateral view explode(array(1,7,30)) tmp as recent_days
    where dt<='2023-01-14' and dt>=date_sub('2023-01-14',recent_days - 1)
    group by recent_days
),active as (
    select recent_days,count(distinct user_id) active_count
    from dwd_user_login_inc
    lateral view explode(array(1,7,30)) tmp as recent_days
    where dt<='2023-01-14' and dt>=date_sub('2023-01-14',recent_days - 1)
    group by recent_days
)
insert overwrite table ads_user_stats
select * from ads_user_stats where dt!='2023-01-14'
union all
select '2023-01-14' dt,
       new.recent_days,
       new.new_count,
       active.active_count
from new
join active on new.recent_days=active.recent_days;

-- 4 用户行为漏斗分析
DROP TABLE IF EXISTS ads_user_action;
CREATE EXTERNAL TABLE ads_user_action
(
    `dt`                STRING COMMENT '统计日期',
    `home_count`        BIGINT COMMENT '浏览首页人数',
    `good_detail_count` BIGINT COMMENT '浏览商品详情页人数',
    `cart_count`        BIGINT COMMENT '加入购物车人数',
    `order_count`       BIGINT COMMENT '下单人数',
    `payment_count`     BIGINT COMMENT '支付人数'
) COMMENT '漏斗分析'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_user_action/';
-- 数据装载
with
pv as (
    select sum(if(page_id='home',1,0)) home_count,
           sum(if(page_id='good_detail',1,0)) good_detail_count
    from (
         select distinct user_id,page_id
         from dwd_traffic_page_view_inc
         where dt='2023-01-14' and (page_id='home' or page_id='good_detail')
    ) t1
),cart as (
    select count(1) cart_count
    from dws_trade_user_cart_add_1d
    where dt='2023-01-14'
),od as (
    select count(1) order_count
    from dws_trade_user_order_1d
    where dt='2023-01-14'
),pay as (
    select count(1) pay_count
    from dws_trade_user_payment_1d
    where dt='2023-01-14'
)
insert overwrite table ads_user_action
select * from ads_user_action where dt!='2023-01-14'
union all
select '2023-01-14',
       home_count,
       good_detail_count,
       cart_count,
       order_count,
       pay_count
from pv
join cart on true
join od on true
join pay on true;

-- 5 新增下单用户统计
DROP TABLE IF EXISTS ads_new_order_user_stats;
CREATE EXTERNAL TABLE ads_new_order_user_stats
(
    `dt`                   STRING COMMENT '统计日期',
    `recent_days`          BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `new_order_user_count` BIGINT COMMENT '新增下单人数'
) COMMENT '新增交易用户统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_new_order_user_stats/';
-- 数据装载
insert overwrite table ads_new_order_user_stats
select * from ads_new_order_user_stats where dt!='2023-01-14'
union all
select '2023-01-14' dt,recent_days,count(user_id)
from (
         select user_id, order_date_first
         from dws_trade_user_order_td
         where dt = '2023-01-14'
     ) t1
lateral view explode(array(1,7,30)) tmp as recent_days
where order_date_first<='2023-01-14' and order_date_first>=date_sub('2023-01-14',recent_days - 1)
group by recent_days;

-- 6 最近7日内连续3日下单用户数
DROP TABLE IF EXISTS ads_order_continuously_user_count;
CREATE EXTERNAL TABLE ads_order_continuously_user_count
(
    `dt`                            STRING COMMENT '统计日期',
    `recent_days`                   BIGINT COMMENT '最近天数,7:最近7天',
    `order_continuously_user_count` BIGINT COMMENT '连续3日下单用户数'
) COMMENT '新增交易用户统计'
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
    LOCATION '/warehouse/gmall/ads/ads_order_continuously_user_count/';
-- 数据装载
insert overwrite table ads_order_continuously_user_count
select * from ads_order_continuously_user_count where dt!='2023-01-14'
union all
select '2023-01-14' dt,7 recent_days,count(distinct user_id)
from (
         select user_id, count(1)
         from (
                  select user_id,
                         dt,
                         date_sub(dt, rn) flag
                  from (
                           select user_id,
                                  dt,
                                  row_number() over (partition by user_id order by dt) rn
                           from dws_trade_user_order_1d
                           where dt <= '2023-01-14'
                             and dt >= date_sub('2023-01-14', 6)
                       ) t1
              ) t2
         group by user_id, flag
         having count(1) >= 3
     ) t3;

-- *******************************************************************************************
--                                          商品主题
-- *******************************************************************************************
-- 1 最近30日各品牌复购率
DROP TABLE IF EXISTS ads_repeat_purchase_by_tm;
CREATE EXTERNAL TABLE ads_repeat_purchase_by_tm
(
    `dt`                STRING COMMENT '统计日期',
    `recent_days`       BIGINT COMMENT '最近天数,30:最近30天',
    `tm_id`             STRING COMMENT '品牌ID',
    `tm_name`           STRING COMMENT '品牌名称',
    `order_repeat_rate` DECIMAL(16, 2) COMMENT '复购率'
) COMMENT '各品牌复购率统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_repeat_purchase_by_tm/';
-- 数据装载
insert overwrite table ads_repeat_purchase_by_tm
select * from ads_repeat_purchase_by_tm where dt!='2023-01-14'
union all
select '2023-01-14' dt,
       30 recent_days,
       tm_id,
       tm_name,
       sum(if(order_count_nd > 1,1,0)) / count(*)
from dws_trade_user_sku_order_nd
where dt='2023-01-14' and recent_days=30
group by tm_id,tm_name;

-- 2 各品牌商品下单统计
DROP TABLE IF EXISTS ads_order_stats_by_tm;
CREATE EXTERNAL TABLE ads_order_stats_by_tm
(
    `dt`                      STRING COMMENT '统计日期',
    `recent_days`             BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `tm_id`                   STRING COMMENT '品牌ID',
    `tm_name`                 STRING COMMENT '品牌名称',
    `order_count`             BIGINT COMMENT '订单数',
    `order_user_count`        BIGINT COMMENT '订单人数'
) COMMENT '各品牌商品交易统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_order_stats_by_tm/';
-- 数据装载
insert overwrite table ads_order_stats_by_tm
select * from ads_order_stats_by_tm where dt != '2023-01-14'
union all
select '2023-01-14' dt,
       recent_days,
       tm_id,
       tm_name,
       sum(order_count_nd),
       count(distinct user_id)
from dws_trade_user_sku_order_nd
where dt='2023-01-14'
group by recent_days,tm_id,tm_name;

-- 3 各品类商品下单统计
DROP TABLE IF EXISTS ads_order_stats_by_cate;
CREATE EXTERNAL TABLE ads_order_stats_by_cate
(
    `dt`                      STRING COMMENT '统计日期',
    `recent_days`             BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `category1_id`            STRING COMMENT '一级分类id',
    `category1_name`          STRING COMMENT '一级分类名称',
    `category2_id`            STRING COMMENT '二级分类id',
    `category2_name`          STRING COMMENT '二级分类名称',
    `category3_id`            STRING COMMENT '三级分类id',
    `category3_name`          STRING COMMENT '三级分类名称',
    `order_count`             BIGINT COMMENT '订单数',
    `order_user_count`        BIGINT COMMENT '订单人数'
) COMMENT '各分类商品交易统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_order_stats_by_cate/';
-- 数据装载
insert overwrite table ads_order_stats_by_cate
select * from ads_order_stats_by_cate where dt!='2023-01-14'
union all
select '2023-01-14' dt,recent_days,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name,
       sum(order_count_nd),
       count(distinct user_id)
from dws_trade_user_sku_order_nd
where dt='2023-01-14'
group by recent_days,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name;

-- 4 各分类商品购物车存量Top3
DROP TABLE IF EXISTS ads_sku_cart_num_top3_by_cate;
CREATE EXTERNAL TABLE ads_sku_cart_num_top3_by_cate
(
    `dt`             STRING COMMENT '统计日期',
    `category1_id`   STRING COMMENT '一级分类ID',
    `category1_name` STRING COMMENT '一级分类名称',
    `category2_id`   STRING COMMENT '二级分类ID',
    `category2_name` STRING COMMENT '二级分类名称',
    `category3_id`   STRING COMMENT '三级分类ID',
    `category3_name` STRING COMMENT '三级分类名称',
    `sku_id`         STRING COMMENT '商品id',
    `sku_name`       STRING COMMENT '商品名称',
    `cart_num`       BIGINT COMMENT '购物车中商品数量',
    `rk`             BIGINT COMMENT '排名'
) COMMENT '各分类商品购物车存量Top3'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_sku_cart_num_top3_by_cate/';
-- 数据装载
with
cate as (
    select id,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name
    from dim_sku_full
    where dt='2023-01-14'
)
insert overwrite table ads_sku_cart_num_top3_by_cate
select * from ads_sku_cart_num_top3_by_cate where dt!='2023-01-14'
union all
select '2023-01-14',
       category1_id,
       category1_name,
       category2_id,
       category2_name,
       category3_id,
       category3_name,
       sku_id,
       sku_name,
       sku_num,
       rn
from (
     select cate.category1_id,
       cate.category1_name,
       cate.category2_id,
       cate.category2_name,
       cate.category3_id,
       cate.category3_name,
       tc.sku_id,
       tc.sku_name,
       tc.sku_num,
       row_number() over (partition by category3_id order by tc.sku_num desc ) rn
    from dwd_trade_cart_full tc
    join cate on tc.sku_id=cate.id
)t1
where rn<=3;

-- 5 各品牌商品收藏次数Top3
DROP TABLE IF EXISTS ads_sku_favor_count_top3_by_tm;
CREATE EXTERNAL TABLE ads_sku_favor_count_top3_by_tm
(
    `dt`          STRING COMMENT '统计日期',
    `tm_id`       STRING COMMENT '品牌id',
    `tm_name`     STRING COMMENT '品牌名称',
    `sku_id`      STRING COMMENT '商品id',
    `sku_name`    STRING COMMENT '商品名称',
    `favor_count` BIGINT COMMENT '被收藏次数',
    `rk`          BIGINT COMMENT '排名'
) COMMENT '各品牌商品收藏次数Top3'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_sku_favor_count_top3_by_tm/';
-- 数据装载
insert overwrite table ads_sku_favor_count_top3_by_tm
select * from ads_sku_favor_count_top3_by_tm where dt!='2023-01-14'
union all
select '2023-01-14',tm_id, tm_name, sku_id, sku_name, favor_add_count_1d, rn
from (
         select tm_id,
                tm_name,
                sku_id,
                sku_name,
                favor_add_count_1d,
                row_number() over (partition by tm_id order by favor_add_count_1d desc) rn
         from dws_interaction_sku_favor_add_1d
         where dt = '2023-01-14'
     ) t1;

-- *******************************************************************************************
--                                          交易主题
-- *******************************************************************************************

-- 1 下单到支付时间时间间隔平均值
DROP TABLE IF EXISTS ads_order_to_pay_interval_avg;
CREATE EXTERNAL TABLE ads_order_to_pay_interval_avg
(
    `dt`                        STRING COMMENT '统计日期',
    `order_to_pay_interval_avg` BIGINT COMMENT '下单到支付时间间隔平均值,单位为秒'
) COMMENT '下单到支付时间间隔平均值'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_order_to_pay_interval_avg/';
-- 数据装载
insert overwrite table ads_order_to_pay_interval_avg
select * from ads_order_to_pay_interval_avg where dt!='2023-01-14'
union all
select '2023-01-14' dt,
       avg(to_unix_timestamp(payment_time) - to_unix_timestamp(order_time))
from dwd_trade_trade_flow_acc
where dt in ('9999-99-99','2023-01-14') and payment_date_id='2023-01-14';

-- 2 各省份交易统计
DROP TABLE IF EXISTS ads_order_by_province;
CREATE EXTERNAL TABLE ads_order_by_province
(
    `dt`                 STRING COMMENT '统计日期',
    `recent_days`        BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `province_id`        STRING COMMENT '省份ID',
    `province_name`      STRING COMMENT '省份名称',
    `area_code`          STRING COMMENT '地区编码',
    `iso_code`           STRING COMMENT '国际标准地区编码',
    `iso_code_3166_2`    STRING COMMENT '国际标准地区编码',
    `order_count`        BIGINT COMMENT '订单数',
    `order_total_amount` DECIMAL(16, 2) COMMENT '订单金额'
) COMMENT '各地区订单统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_order_by_province/';
-- 数据装载
insert overwrite table ads_order_by_province
select * from ads_order_by_province where dt!='2023-01-14'
union all
select '2023-01-14' dt,
       recent_days,
       province_id,
       province_name,
       area_code,
       iso_code,
       iso_3166_2,
       order_count_nd,
       order_total_amount_nd
from dws_trade_province_order_nd
where dt='2023-01-14';

-- *******************************************************************************************
--                                          优惠券主题
-- *******************************************************************************************

-- 优惠券使用统计
DROP TABLE IF EXISTS ads_coupon_stats;
CREATE EXTERNAL TABLE ads_coupon_stats
(
    `dt`              STRING COMMENT '统计日期',
    `coupon_id`       STRING COMMENT '优惠券ID',
    `coupon_name`     STRING COMMENT '优惠券名称',
    `used_count`      BIGINT COMMENT '使用次数',
    `used_user_count` BIGINT COMMENT '使用人数'
) COMMENT '优惠券统计'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/warehouse/gmall/ads/ads_coupon_stats/';
-- 数据装载
insert overwrite table ads_coupon_stats
select * from ads_coupon_stats where dt!='2023-01-14'
union all
select '2023-01-14' dt,coupon_id,coupon_name,
       sum(used_count_1d),
       count(distinct user_id)
from dws_tool_user_coupon_coupon_used_1d
where dt='2023-01-14'
group by coupon_id,coupon_name;
