#!/bin/bash
#1.参数个数判断
if [ $# -lt 1 ]
then
 echo "请输入正确参数all/表名" && exit
fi
#2.根据表名导数据到mysql
function export_data(){
 python /opt/module/datax/bin/datax.py -p"-Dexportdir=/warehouse/gmall/ads/$1/*" /opt/module/datax/job/export/gmall_report.$1.json
}
case $1 in
"all")
 export_data ads_coupon_stats
 export_data ads_new_order_user_stats
 export_data ads_order_by_province
 export_data ads_order_continuously_user_count
 export_data ads_order_stats_by_cate
 export_data ads_order_stats_by_tm
 export_data ads_order_to_pay_interval_avg
 export_data ads_page_path
 export_data ads_repeat_purchase_by_tm
 export_data ads_sku_cart_num_top3_by_cate
 export_data ads_sku_favor_count_top3_by_tm
 export_data ads_traffic_stats_by_channel
 export_data ads_user_action
 export_data ads_user_change
 export_data ads_user_retention
 export_data ads_user_stats
;;
"ads_coupon_stats")
    export_data ads_coupon_stats
;;
"ads_new_order_user_stats")
    export_data ads_new_order_user_stats
;;
"ads_order_by_province")
    export_data ads_order_by_province
;;
"ads_order_continuously_user_count")
    export_data ads_order_continuously_user_count
;;
"ads_order_stats_by_cate")
    export_data ads_order_stats_by_cate
;;
"ads_order_stats_by_tm")
    export_data ads_order_stats_by_tm
;;
"ads_order_to_pay_interval_avg")
    export_data ads_order_to_pay_interval_avg
;;
"ads_page_path")
    export_data ads_page_path
;;
"ads_repeat_purchase_by_tm")
    export_data ads_repeat_purchase_by_tm
;;
"ads_sku_cart_num_top3_by_cate")
    export_data ads_sku_cart_num_top3_by_cate
;;
"ads_sku_favor_count_top3_by_tm")
    export_data ads_sku_favor_count_top3_by_tm
;;
"ads_traffic_stats_by_channel")
    export_data ads_traffic_stats_by_channel
;;
"ads_user_action")
    export_data ads_user_action
;;
"ads_user_change")
    export_data ads_user_change
;;
"ads_user_retention")
    export_data ads_user_retention
;;
"ads_user_stats")
    export_data ads_user_stats
;;
*)
 echo "请输入正确参数all/表名"
;;
esac



