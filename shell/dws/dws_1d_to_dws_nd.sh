#!/bin/bash
# 作用：DWS层（n日汇总），首日/每日数据装载（DWD -> DWS_nd）

# 1.判断参数
if [ $# -lt 1 ]
then
	echo "请输入正确参数表名/all" && exit
fi

# 2.定时时间属性
[ "$2" ] && dateStr=$2 || dateStr=`date -d '-1 day' +%F`

# 3.执行导数SQL
dws_trade_province_order_nd="insert overwrite table dws_trade_province_order_nd partition (dt='${dateStr}')
select province_id,province_name,area_code,iso_code,iso_3166_2,recent_days,
       sum(order_count_1d),
       sum(order_original_amount_1d),
       sum(activity_reduce_amount_1d),
       sum(coupon_reduce_amount_1d),
       sum(order_total_amount_1d)
from dws_trade_province_order_1d
lateral view explode(array(1,7,30)) tmp as recent_days
where dt<='${dateStr}' and dt>=date_sub('${dateStr}',recent_days - 1)
group by province_id,province_name,area_code,iso_code,iso_3166_2,recent_days;"
dws_trade_user_sku_order_nd="insert overwrite table dws_trade_user_sku_order_nd partition (dt='${dateStr}')
select user_id,sku_id,sku_name,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name,tm_id,tm_name,recent_days,
       sum(order_count_1d) flag,
       sum(order_num_1d),
       sum(order_original_amount_1d),
       sum(activity_reduce_amount_1d),
       sum(coupon_reduce_amount_1d),
       sum(order_total_amount_1d)
from dws_trade_user_sku_order_1d
lateral view explode(array(1,7,30)) tmp as recent_days
where dt<='${dateStr}' and dt>=date_sub('${dateStr}',recent_days - 1)
group by user_id,sku_id,sku_name,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name,tm_id,tm_name,recent_days;"

case $1 in
"all")
	$HIVE_HOME/bin/hive -e "${dws_trade_province_order_nd}${dws_trade_user_sku_order_nd}"
;;
"dws_trade_province_order_nd")
    $HIVE_HOME/bin/hive -e "${dws_trade_province_order_nd}"
;;
"dws_trade_user_sku_order_nd")
    $HIVE_HOME/bin/hive -e "${dws_trade_user_sku_order_nd}"
;;
esac
