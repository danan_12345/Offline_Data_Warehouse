#!/bin/bash
# 作用：DWS层（历史至今汇总），每日数据装载（DWD -> DWS_td）

if [ $# -ne 1 ]
then
	echo "请输入正确参数表名/all"  && exit
fi

dws_trade_user_order_td="insert overwrite table dws_trade_user_order_td partition (dt='2023-01-14')
select user_id,
       min(dt),
       max(dt),
       sum(order_count_1d),
       sum(order_num_1d),
       sum(order_original_amount_1d),
       sum(activity_reduce_amount_1d),
       sum(coupon_reduce_amount_1d),
       sum(order_total_amount_1d)
from dws_trade_user_order_1d
where dt<='2023-01-14'
group by user_id;"
dws_user_user_login_td="with
ui as (
    select id,create_time
    from dim_user_zip
    where dt='9999-99-99'
),login as (
    select user_id,
           max(date_id) last_login,
           count(1) login_count
    from dwd_user_login_inc
    where dt='2023-01-14'
    group by user_id
)
insert overwrite table dws_user_user_login_td partition (dt='2023-01-14')
select ui.id,
       nvl(login.last_login,date_format(create_time,'yyyy-MM-dd')),
       nvl(login.login_count,1)
from ui
left join login on ui.id=login.user_id;"

case $1 in
"all")
	$HIVE_HOME/bin/hive -e "${dws_trade_user_order_td}${dws_user_user_login_td}"
;;
"dws_trade_user_order_td")
    $HIVE_HOME/bin/hive -e "${dws_trade_user_order_td}"
;;
"dws_user_user_login_td")
    $HIVE_HOME/bin/hive -e "${dws_user_user_login_td}"
;;
esac

