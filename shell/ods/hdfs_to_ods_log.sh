#!/bin/bash

# 作用：日志数据，每日装载脚本

# 1.给日期属性赋值
[ "$1" ] && dateStr=$1 || dateStr=$(date -d '-1 day' +%F)
# 2.进行数据装载
$HIVE_HOME/bin/hive -e "load data inpath '/origin_data/gmall/log/topic_log/${dateStr}' overwrite into table ods_log_inc partition (dt='${dateStr}')"
