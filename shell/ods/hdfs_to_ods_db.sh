#!/bin/bash
# 作用：业务数据，每日装载脚本

# 1.判断是否输入参数
if [ $# -lt 1 ]
then
	echo "请输入表名/all" && exit
fi

# 2.定义时间属性
[ "$2" ] && dateStr=$2 || dateStr=$(date -d '-1 day' +%F)

# 3.数据装载
load_data(){
	tables=$*
	sql="use default"
	for table in $*
	do
		sql="${sql};load data inpath '/origin_data/gmall/db/${table:4}/${dateStr}' overwrite into table ${table} partition (dt='${dateStr}')"
	done
	$HIVE_HOME/bin/hive -e "$sql"
}

case $1 in
"all")
	load_data "ods_activity_info_full" "ods_activity_rule_full" "ods_base_category1_full" "ods_base_category2_full" "ods_base_category3_full" "ods_base_dic_full" "ods_base_province_full" "ods_base_region_full" "ods_base_trademark_full" "ods_cart_info_full" "ods_cart_info_inc" "ods_comment_info_inc" "ods_coupon_info_full" "ods_coupon_use_inc" "ods_favor_info_inc" "ods_order_detail_activity_inc" "ods_order_detail_coupon_inc" "ods_order_detail_inc" "ods_order_info_inc" "ods_order_refund_info_inc" "ods_order_status_log_inc" "ods_payment_info_inc" "ods_refund_payment_inc" "ods_sku_attr_value_full" "ods_sku_info_full" "ods_sku_sale_attr_value_full" "ods_spu_info_full" "ods_user_info_inc"
;;
"ods_activity_info_full")
    load_data "ods_activity_info_full"
;;
"ods_activity_rule_full")
    load_data "ods_activity_rule_full"
;;
"ods_base_category1_full")
    load_data "ods_base_category1_full"
;;
"ods_base_category2_full")
    load_data "ods_base_category2_full"
;;
"ods_base_category3_full")
    load_data "ods_base_category3_full"
;;
"ods_base_dic_full")
    load_data "ods_base_dic_full"
;;
"ods_base_province_full")
    load_data "ods_base_province_full"
;;
"ods_base_region_full")
    load_data "ods_base_region_full"
;;
"ods_base_trademark_full")
    load_data "ods_base_trademark_full"
;;
"ods_cart_info_full")
    load_data "ods_cart_info_full"
;;
"ods_cart_info_inc")
    load_data "ods_cart_info_inc"
;;
"ods_comment_info_inc")
    load_data "ods_comment_info_inc"
;;
"ods_coupon_info_full")
    load_data "ods_coupon_info_full"
;;
"ods_coupon_use_inc")
    load_data "ods_coupon_use_inc"
;;
"ods_favor_info_inc")
    load_data "ods_favor_info_inc"
;;
"ods_order_detail_activity_inc")
    load_data "ods_order_detail_activity_inc"
;;
"ods_order_detail_coupon_inc")
    load_data "ods_order_detail_coupon_inc"
;;
"ods_order_detail_inc")
    load_data "ods_order_detail_inc"
;;
"ods_order_info_inc")
    load_data "ods_order_info_inc"
;;
"ods_order_refund_info_inc")
    load_data "ods_order_refund_info_inc"
;;
"ods_order_status_log_inc")
    load_data "ods_order_status_log_inc"
;;
"ods_payment_info_inc")
    load_data "ods_payment_info_inc"
;;
"ods_refund_payment_inc")
    load_data "ods_refund_payment_inc"
;;
"ods_sku_attr_value_full")
    load_data "ods_sku_attr_value_full"
;;
"ods_sku_info_full")
    load_data "ods_sku_info_full"
;;
"ods_sku_sale_attr_value_full")
    load_data "ods_sku_sale_attr_value_full"
;;
"ods_spu_info_full")
    load_data "ods_spu_info_full"
;;
"ods_user_info_inc")
    load_data "ods_user_info_inc"
;;
esac
